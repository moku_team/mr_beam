SimpleCov.start do
    add_group 'lib', 'lib'
    require 'simplecov-shield'
    SimpleCov.formatter = SimpleCov::Formatter::ShieldFormatter
    SimpleCov::Formatter::ShieldFormatter.config[:style] = :flat
    SimpleCov::Formatter::ShieldFormatter.config[:precision] = 2

end
