require 'spec_helper'
require 'mr_beam/modules/shared_folders/shared_folders_file_channel'  

RSpec.describe SharedFolders::SharedFoldersFileChannel, type: :model do
  describe 'when initialising a new channel' do
    before do
      @subject_object = 'subject_object'
      @channel_hash = {
          subject: @subject_object,
          config: {
              input_folder: 'input',
              output_folder: 'output',
              failed_folder: 'failed',
              input_file_name: 'input_file',
              output_file_name: 'output_file',
              should_overwrite_dest: false,
              prevent_start: true
          }
      }
      @block = lambda { |a| a }
    end
    it 'should initialise properly' do
      channel = MrBeam::SharedFoldersFileChannel.new @channel_hash, @block

      expect(channel.subject).to eq @subject_object
      expect(channel.message_queue).to be_empty
      expect(channel.input_folder).to eq 'input'
      expect(channel.output_folder).to eq 'output'
      expect(channel.failed_folder).to eq 'failed'
      expect(channel.input_file_name).to eq 'input_file'
      expect(channel.output_file_name).to eq 'output_file'
      expect(channel.should_overwrite_dest).to be false
      expect(channel.receive_callback_block).to eq @block

      #TODO I might add a check for the Thread to have started, but we'll see
    end
  end

  describe 'when processing an input folder with a valid input file' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_folder_processing_testing)
      fixture = 'spec/fixtures/input.txt'
      input_folder =   'spec/support/shared_folders/input'
      input_file_name = 'input.txt'
      @input_file = "#{input_folder}/#{input_file_name}"
      FileUtils.copy fixture, @input_file

      @delegate = double('delegate')
      @channel.receive_callback_block = lambda { |msg| @delegate.invoke(msg) }
    end

    it 'should detect the file, delete it and call the callback block' do
      expect(@delegate).to receive(:invoke).with('Example content')
      @channel.process_folder

      expect(Dir[@input_file]).to be_empty
    end

    after do
      FileUtils.rm_f @input_file
    end
  end

  describe 'when processing an input folder without a valid input file' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_folder_processing_testing)
      fixture = 'spec/fixtures/input.txt'
      input_folder = 'spec/support/shared_folders/input'
      input_file_name = 'wrong_input.txt'
      @input_file = "#{input_folder}/#{input_file_name}"
      FileUtils.copy fixture, @input_file
    end

    it 'should do nothing' do
      @channel.process_folder

      expect(Dir[@input_file].length).to eq 1
    end

    after do
      FileUtils.rm_f @input_file
    end
  end

  describe 'when processing an input folder with a valid input file but a broken receive callback' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_folder_processing_testing)
      fixture = 'spec/fixtures/input.txt'
      input_folder = 'spec/support/shared_folders/input'
      failed_folder = 'spec/support/shared_folders/failed'
      input_file_name = 'input.txt'
      @input_file = "#{input_folder}/#{input_file_name}"
      @failed_file = "#{failed_folder}/#{input_file_name}"
      FileUtils.copy fixture, @input_file
      @channel.receive_callback_block = lambda { raise StandardError, 'Callback error' }
    end

    it 'should move the file to failed folder' do
      begin
        @channel.send(:process_queue)
      rescue StandardError => e
        expect(e.message).to eq 'Callback error'
      end
      expect(Dir[@failed_file].length).to eq 1
      expect(Dir[@input_file]).to be_empty
    end

    after do
      FileUtils.rm_f @failed_file
    end
  end

  describe 'when sending a message' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_queue_processing_testing)
      output_folder = 'spec/support/shared_folders/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"
      @delegate = double('delegate')
      @example_message = {
          message: 'Example message',
          block: lambda { |outcome| @delegate.invoke(outcome) }
      }
    end

    it 'should write the file in the right folder and call the transmit callback' do
      expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)

      @channel.send(:send_message, @example_message)

      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Example message'
    end

    after do
      FileUtils.rm_f @output_file
    end
  end

  describe 'when processing a non-empty message queue with the overwrite option = true' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_queue_processing_testing_overwrite)
      output_folder = 'spec/support/shared_folders/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"

      @delegate = double('delegate')
      @channel.message_queue << {
          message: 'Oldest example message',
          block: lambda { |outcome| @delegate.invoke(outcome) }
      }
      @channel.message_queue << {
          message: 'More recent example message',
          block: nil
      }
      @channel.message_queue << {
          message: 'Even more recent example message',
        block: nil
      }

      fixture = 'spec/fixtures/input.txt'
      FileUtils.copy fixture, @output_file
    end

    it 'should extract the oldest message and write it as a file in the correct folder, no matter if there is a file' do
      expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)
      @channel.send(:process_queue)
      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Oldest example message'
      expect(@channel.message_queue.length).to eq 2
    end

    after do
      FileUtils.rm_f @output_file
    end
  end

  describe 'when processing a non-empty message queue with the overwrite option = false' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_queue_processing_testing_no_overwrite)
      output_folder = 'spec/support/shared_folders/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"
      @delegate = double('delegate')
      @channel.message_queue << {
          message: 'Oldest example message',
          block: lambda { |outcome| @delegate.invoke(outcome) }
      }
      @channel.message_queue << {
          message: 'More recent example message',
          block: nil
      }
      @channel.message_queue << {
          message: 'Even more recent example message',
          block: nil
      }
      fixture = 'spec/fixtures/input.txt'
      FileUtils.copy fixture, @output_file
    end

    it 'should do nothing if there\'s a file already there' do
      @channel.send(:process_queue)
      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Example content'
      expect(@channel.message_queue.length).to eq 3
    end

    it 'should extract the oldest message and write it as a file in the correct folder if there\'s no file already there' do
      FileUtils.rm_f @output_file
      expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)
      @channel.send(:process_queue)
      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Oldest example message'
      expect(@channel.message_queue.length).to eq 2
    end

    after do
      FileUtils.rm_f @output_file
    end
  end

  describe 'when processing an empty message queue' do
    before do
      @channel = FactoryGirl.build(:shared_folders_file_channel_for_queue_processing_testing)
      output_folder = 'spec/support/shared_folders/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"
    end

    it 'should do nothing' do
      @channel.send(:process_queue)

      expect(@channel.message_queue).to be_empty
      expect(Dir[@output_file]).to be_empty
    end

    after do
      FileUtils.rm_f @output_file
    end
  end
end
