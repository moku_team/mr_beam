require 'yaml'
require 'rspec'
require 'simplecov'

require 'factory_girl'
FactoryGirl.find_definitions

RSpec::Matchers.define :a_successful_transmit_outcome do
  match { |actual| actual.success? }
end

require 'mr_beam'
