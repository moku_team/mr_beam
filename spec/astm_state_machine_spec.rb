require 'spec_helper'
require 'mr_beam/astm_state_machine'




RSpec.describe MrBeam::AstmStateMachine, type: :model do


#TODO Move into correct partial
  ###+------------------+###***
  ###+-----TRANSMIT-----+###***
  ###+------------------+###***
  describe 'when state machine has something to transmit over the connection' do
    before do
      io_object = double(IO)

      @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
      allow(@astm_state_machine.current_state_instance).to receive(:read) {nil}
      @astm_state_machine.channel_busy = false
      @astm_state_machine.tx_buffer << { message: 'Something to transmit out of here', block: nil }
    end

    it 'should go on DataToSendState' do
      #PRECONDITION fullfilled: there is something to deliver, channel is not busy and previous state was Idle
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.current_state_instance.send 'something_to_send?').to eq true
      @astm_state_machine.current_state_instance.next
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::SendBranch::DataToSend
    end
  end
end

