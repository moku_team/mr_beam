require 'mr_beam/file_channel'
FactoryGirl.define do
  factory :file_channel, :class => MrBeam::FileChannel do
    skip_create
    initialize_with { new(attributes, nil) }
    factory :empty_file_channel do
      config {{
        :input_folder => '',
        :output_folder => '',
        :failed_folder => '',
        :input_file_name => '',
        :output_file_name => '',
        :should_overwrite_dest => ''
      }}
      sequence(:subject) { 'subject' }
      sequence(:message_queue) { [] }
    end
  end
end