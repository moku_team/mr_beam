require 'mr_beam/modules/ftp/ftp_file_channel'
require 'testftpd'

FactoryGirl.define do
  factory :ftp_file_channel, :class => Ftp::FtpFileChannel do
    skip_create
    initialize_with { new(attributes, nil) }

    factory :ftp_file_channel_for_folder_processing_testing do
      config {{
          :input_folder => 'input',
          :output_folder => 'output',
          :input_file_name => 'input.txt',
          :output_file_name => 'output.txt',
          :should_overwrite_dest => '',
          :processing_folder => 'spec/support/ftp/processing',
          :server => '127.0.0.1',
          :port => '21212',
          :username => 'username',
          :password => 'password',
          :prevent_start => true
      }}

      sequence(:subject) { 'subject1' }
    end

    factory :ftp_file_channel_for_queue_processing_testing do
      config {{
          :input_folder => 'input',
          :output_folder => 'output',
          :input_file_name => 'input.txt',
          :output_file_name => 'output.txt',
          :should_overwrite_dest => '',
          :processing_folder => 'spec/support/ftp/processing',
          :server => '127.0.0.1',
          :port => '21212',
          :username => 'username',
          :password => 'password',
          :prevent_start => true
      }}
      sequence(:subject) { 'subject1' }

      factory :ftp_file_channel_for_queue_processing_testing_overwrite do
        config {{
            :input_folder => 'input',
            :output_folder => 'output',
            :input_file_name => 'input.txt',
            :output_file_name => 'output.txt',
            :should_overwrite_dest => true,
            :processing_folder => 'spec/support/ftp/processing',
            :server => '127.0.0.1',
            :port => '21212',
            :username => 'username',
            :password => 'password',
            :prevent_start => true
        }}
      end

      factory :ftp_file_channel_for_queue_processing_testing_no_overwrite do
        config {{
            :input_folder => 'input',
            :output_folder => 'output',
            :input_file_name => 'input.txt',
            :output_file_name => 'output.txt',
            :should_overwrite_dest => false,
            :processing_folder => 'spec/support/ftp/processing',
            :server => '127.0.0.1',
            :port => '21212',
            :username => 'username',
            :password => 'password',
            :prevent_start => true
        }}
      end
    end
  end
end