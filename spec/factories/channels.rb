require 'mr_beam/channel'
FactoryGirl.define do
  factory :channel, :class => MrBeam::Channel do
    skip_create
    initialize_with { new(attributes, nil) }
    factory :channel_with_example_subject do
      sequence(:subject) { 'subject_object' }
    end
  end
end