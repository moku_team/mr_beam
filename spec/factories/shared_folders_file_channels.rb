require 'mr_beam/modules/shared_folders/shared_folders_file_channel'
FactoryGirl.define do
  factory :shared_folders_file_channel, :class => SharedFolders::SharedFoldersFileChannel  do
    skip_create
    initialize_with { new(attributes, nil) }

    factory :shared_folders_file_channel_for_folder_processing_testing do
      config {{
          :input_folder => 'spec/support/shared_folders/input',
          :output_folder => 'spec/support/shared_folders/output',
          :input_file_name => 'input.txt',
          :output_file_name => 'output.txt',
          :should_overwrite_dest => '',
          :prevent_start => true
      }}
      sequence(:subject) { 'subject1' }
    end

    factory :shared_folders_file_channel_for_queue_processing_testing do

      config {{
          :input_folder => 'spec/support/shared_folders/input',
          :output_folder => 'spec/support/shared_folders/output',
          :input_file_name => 'input.txt',
          :output_file_name => 'output.txt',
          :should_overwrite_dest => '',
          :prevent_start => true
      }}

      factory :shared_folders_file_channel_for_queue_processing_testing_overwrite do
        config {{
            :input_folder => 'spec/support/shared_folders/input',
            :output_folder => 'spec/support/shared_folders/output',
            :input_file_name => 'input.txt',
            :output_file_name => 'output.txt',
            :should_overwrite_dest => true,
            :prevent_start => true
        }}
      end

      factory :shared_folders_file_channel_for_queue_processing_testing_no_overwrite do
        config {{
            :input_folder => 'spec/support/shared_folders/input',
            :output_folder => 'spec/support/shared_folders/output',
            :input_file_name => 'input.txt',
            :output_file_name => 'output.txt',
            :should_overwrite_dest => false,
            :prevent_start => true
        }}
      end
    end
  end
end