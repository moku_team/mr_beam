require 'spec_helper'
require 'mr_beam/object_refinements'

class ObjectRefinementsTesterClass
  using MrBeam::ObjectRefinements
  
  def self.blank_tester _something_
    _something_.blank?
  end
  
  def self.present_tester _something_
    _something_.present?
  end

end


RSpec.describe MrBeam::ObjectRefinements, type: :module do
  describe 'when a class use the MrBeam::ObjectRefinement module' do
    
    it "nil should be blank" do
      expect(ObjectRefinementsTesterClass.blank_tester(nil)).to be true
    end
    
    it "empty string should be blank" do
      expect(ObjectRefinementsTesterClass.blank_tester('')).to be true
    end
    
    it "empty object should be blank" do
      expect(ObjectRefinementsTesterClass.blank_tester({})).to be true
    end
    
    it "empty array should be blank" do
      expect(ObjectRefinementsTesterClass.blank_tester([])).to be true
    end

    it "nil should not be present" do
      expect(ObjectRefinementsTesterClass.present_tester(nil)).to_not be true
    end

    it "empty string should not be present" do
      expect(ObjectRefinementsTesterClass.present_tester('')).to_not be true
    end

    it "empty object should not be present" do
      expect(ObjectRefinementsTesterClass.present_tester({})).to_not be true
    end

    it "empty array should not be present" do
      expect(ObjectRefinementsTesterClass.present_tester([])).to_not be true
    end

    it "not empty string should be present" do
      expect(ObjectRefinementsTesterClass.present_tester('test')).to be true
    end

    it "fixnum should be present" do
      expect(ObjectRefinementsTesterClass.present_tester(1)).to be true
    end

  end
end
