require 'mr_beam/astm_state_machine'
# 'when state machine has nothing to transmit so works on reading mode'

describe 'when state machine receives an enquire' do

  #+-------------------------+#
  #+------DATA_TO_SEND-------+#
  #+-------------------------+#


















  before :each do
    #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

    sample_message = File.read('spec/fixtures/astm/correct_frame.binary')
    @payload = File.read('spec/fixtures/astm/correct_frame_payload_only.binary')
    # first_substring = sample_message.split(IoConstants::CR.chr).first

    io_channed_delegate = double(IO, received: true)
    io_object = double(IO, putc: IoConstants::ACK)
    @message_counter = 0
    allow(io_object).to receive(:readbyte) do
      # :nocov:
      to_return = sample_message[@message_counter]
      @message_counter += 1
      to_return
      # :nocov:
    end

    #I must create a system that make possible to set ready true after a while
    current_time = Time.now
    allow(io_object).to receive(:ready?) {Time.now < current_time + 1}


    @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate


    @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
    @astm_state_machine.current_read_message = sample_message
    @astm_state_machine.next
    #second next is needed because the working logic is weapped on next method
    @astm_state_machine.next
  end

  it 'payload should be correct' do
    expect(@astm_state_machine.payload).to eq @payload
  end


  context 'when channel is free' do
    context 'something to send on tx buffer'do
      before do
          io_object = double(IO, putc: IoConstants::ENQ)
          @sample_message= ''
          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            # :nocov:
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
            # :nocov:
          end

          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
          @astm_state_machine.tx_buffer << { message: 'It\'s me, Máriò!', block: nil }
          @astm_state_machine.channel_busy= false

          @astm_state_machine.set_state AstmStates::SendBranch::DataToSend
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
      end

      it 'should go form data_to_send to waiting state' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::SendBranch::Waiting
        expect(@astm_state_machine.channel_busy? ).to eq false
        expect(@astm_state_machine.frame_number).to eq 1
        expect(@astm_state_machine.timeout_in_seconds ).to eq 15

      end

    end
  end



  # +------------------------------------------------------------------------------------------------------------------+

  context 'when channel is busy' do
    before do
      io_object = double(IO, putc: IoConstants::ENQ)
      @sample_message= ''
      @message_counter = 0
      allow(io_object).to receive(:readbyte) do
        # :nocov:
        to_return = @sample_message[@message_counter]
        @message_counter += 1
        to_return
        # :nocov:
      end

      #I must create a system that make possible to set ready true after a while
      current_time = Time.now
      allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

      @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
      @astm_state_machine.tx_buffer << { message: 'It\'s me, Máriò!', block: nil }
      @astm_state_machine.channel_busy= true

      @astm_state_machine.set_state AstmStates::SendBranch::DataToSend
      @astm_state_machine.current_read_message = @sample_message
      @astm_state_machine.next
    end

    it 'should go form data_to_send to idle state' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.channel_busy? ).to eq true
    end
  end

  # +------------------------------------------------------------------------------------------------------------------+

  context 'when tx_buffer is empty' do
    before do
      io_object = double(IO, putc: IoConstants::ENQ)
      @sample_message= ''
      @message_counter = 0
      allow(io_object).to receive(:readbyte) do
        # :nocov:
        to_return = @sample_message[@message_counter]
        @message_counter += 1
        to_return
        # :nocov:
      end

      #I must create a system that make possible to set ready true after a while
      current_time = Time.now
      allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

      @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
      @astm_state_machine.tx_buffer = []
      @astm_state_machine.channel_busy= false

      @astm_state_machine.set_state AstmStates::SendBranch::DataToSend
      @astm_state_machine.current_read_message = @sample_message
      @astm_state_machine.next
    end

    it 'should go form data_to_send to idle state' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.channel_busy? ).to eq false
    end
  end

end
