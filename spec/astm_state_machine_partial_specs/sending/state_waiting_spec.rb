require 'mr_beam/astm_state_machine'
# 'when state machine has nothing to transmit so works on reading mode'

describe 'when state machine goes on SendBranch::Waiting state' do

  #+-------------------------+#
  #+---------WAITING---------+#
  #+-------------------------+#


  context 'received NACK' do
    before do
        io_object = double(IO)
        @sample_message= IoConstants::NACK.chr

        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = @sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil

        @astm_state_machine.set_state AstmStates::SendBranch::Waiting
        @astm_state_machine.current_read_message = @sample_message
        @astm_state_machine.next
    end

    it 'should go back to idle' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.channel_busy?).to eq false
    end

  end


  context 'received ENQ' do
    before do
        io_object = double(IO)
        @sample_message= IoConstants::ENQ.chr

        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = @sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil

        @astm_state_machine.set_state AstmStates::SendBranch::Waiting
        @astm_state_machine.current_read_message = @sample_message
        @astm_state_machine.next
    end

    it 'should go back to idle' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.channel_busy?).to eq false
    end

  end


  context 'received ACK' do
    before do
        io_object = double(IO)
        @sample_message= IoConstants::ACK.chr

        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = @sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil

        @astm_state_machine.set_state AstmStates::SendBranch::Waiting
        @astm_state_machine.current_read_message = @sample_message
        @astm_state_machine.next
    end

    it 'should go on SendBranc::NextFrameSetUp state with channel free' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::SendBranch::NextFrameSetUp
      expect(@astm_state_machine.channel_busy?).to eq false
      expect(@astm_state_machine.sending_retries_count).to eq 0
    end

  end

  context 'timout expired' do
    before do
        io_object = double(IO, putc: IoConstants::EOT)
        @sample_message= 'It\'s the final countdown tu tutu tu tuuu tutututuuu tututututututuuu tutututuuu tututuuu uu uuuuu'

        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = @sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        allow(@astm_state_machine).to receive(:timeout_expired?) {true}
        @astm_state_machine.set_state AstmStates::SendBranch::Waiting
        @astm_state_machine.current_read_message = @sample_message
        @astm_state_machine.next
    end

    it 'should go back to idle' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.channel_busy?).to eq false
    end

  end

end
