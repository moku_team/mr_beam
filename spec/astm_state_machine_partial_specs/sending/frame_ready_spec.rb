require 'mr_beam/astm_state_machine'
# 'when state machine has nothing to transmit so works on reading mode'

describe 'when state machine goes on SendBranch::Waiting state' do

  #+-----------------------------------+#
  #+------------FRAME_READY------------+#
  #+-----------------------------------+#


    context 'sending frame' do

      before do


        @sample_message= 'burrito, mojito and the party goes on fire'
        io_object = double(IO)
        allow(io_object).to receive(:print).with(@sample_message) {true}

        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        @astm_state_machine.set_state AstmStates::SendBranch::FrameReady
        @astm_state_machine.current_frame_to_send = @sample_message


        @astm_state_machine.tx_buffer = [
            { message: 'message in a brandy bottle', block: nil },
            { message: 'costanzo\'s neck', block: nil }
        ]
        @astm_state_machine.current_array_of_frame_to_send = []
        @astm_state_machine.next
      end

      it 'should goes on WaitingWriteResponse state' do
         expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::SendBranch::WaitingWriteResponse
         expect(@astm_state_machine.timeout_in_seconds).to eq 15
      end

    end






end
