require 'mr_beam/astm_state_machine'
# 'when state machine has nothing to transmit so works on reading mode'

describe 'when state machine goes on SendBranch::Waiting state' do

  #+-----------------------------------+#
  #+---------NEXT_FRAME_SET_UP---------+#
  #+-----------------------------------+#


  context 'message building' do

    # describe '' do
    #
    #   io_object = double(IO)
    # end

    context 'message fully processed' do

      before do
        io_object = double(IO, putc: IoConstants::EOT)
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        @astm_state_machine.set_state AstmStates::SendBranch::NextFrameSetUp
        @delegate = double()
        @astm_state_machine.tx_buffer = [
            { message: 'message in a brandy bottle', block: lambda { |a| @delegate.invoke(a) } },
            { message: 'costanzo\'s neck', block: nil }
        ]
        @astm_state_machine.current_array_of_frame_to_send = []
      end

      it 'test' do
        expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)
        @astm_state_machine.next
        expect(@astm_state_machine.tx_buffer).to eq [{ message: 'costanzo\'s neck', block: nil }]
        expect(@astm_state_machine.current_array_of_frame_to_send).to eq []
        expect(@astm_state_machine.current_frame_to_send).to eq nil
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      end

    end




    context 'message partially processed' do

      before do
        io_object = double(IO, putc: IoConstants::EOT)
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        @astm_state_machine.set_state AstmStates::SendBranch::NextFrameSetUp
        @astm_state_machine.tx_buffer = [
            { message: 'message in a brandy bottle', block: nil },
            { message: 'costanzo\'s neck', block: nil }
        ]
        @astm_state_machine.current_array_of_frame_to_send = ['a', 'b', 'c']
        @astm_state_machine.next
      end

      it 'test' do
        expect(@astm_state_machine.tx_buffer).to eq [
                                                        { message: 'message in a brandy bottle', block: nil },
                                                        { message: 'costanzo\'s neck', block: nil }
                                                    ]
        expect(@astm_state_machine.current_array_of_frame_to_send).to eq ['a', 'b', 'c']
        expect(@astm_state_machine.current_frame_to_send).to eq 'a'
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::SendBranch::FrameReady
      end

    end




  end


end
