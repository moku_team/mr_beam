require 'mr_beam/astm_state_machine'
# 'when state machine has nothing to transmit so works on reading mode'

context 'when state machine goes on SendBranch::OldFrameSetUp state' do

  #+-----------------------------------+#
  #+---------NEXT_FRAME_SET_UP---------+#
  #+-----------------------------------+#


  context 'fail handling' do


    describe 'retries >= 6' do

      before do
        io_object = double(IO, putc: IoConstants::EOT)
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        @astm_state_machine.set_state AstmStates::SendBranch::OldFrameSetUp
        @astm_state_machine.sending_retries_count = 6
        @astm_state_machine.next
      end

      it 'test' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      end

    end




    describe 'retries < 6' do

      before do
        io_object = double(IO,putc: IoConstants::EOT)
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        @astm_state_machine.set_state AstmStates::SendBranch::OldFrameSetUp
        @astm_state_machine.sending_retries_count = 5
        @astm_state_machine.next
      end

      it 'test' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::SendBranch::FrameReady
      end

    end

  end
end
