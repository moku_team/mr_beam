require 'mr_beam/modules/astm_states/astm_states'
require 'byebug'

###+------------------------+###***
###+-------BASE_STATE-------+###***
###+------------------------+###***


describe 'base state methods test' do

  it 'string_between_markers should work fine' do
    substring_ = BaseAstmState.string_between_markers 'BEGINGianfilippo e le giornate uggiose. Presenta Weed De Filippi.END', 'BEGIN', 'END'
    expect(substring_).to eq 'Gianfilippo e le giornate uggiose. Presenta Weed De Filippi.'
  end

  context 'current_read_message_contains_the_byte? should work fine' do
    before :each do
      io_object = double(IO)
      @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
      @astm_state_machine.set_state AstmStates::Idle
      @astm_state_machine.current_read_message =  'hasta la vista' + IoConstants::EOT.chr + ' with burritos'
    end


    it 'should match' do
      expect(@astm_state_machine.current_state_instance.current_read_message_contains_the_byte? IoConstants::EOT).to eq true
    end

    it 'should not match' do
      expect(@astm_state_machine.current_state_instance.current_read_message_contains_the_byte? IoConstants::ACK).to eq false
    end

  end

  # +----------------------------------------------------------------------------------------------------------------+

  context 'checksum on STX >> ETB' do

    before do
      @message = '' << IoConstants::STX.chr << 31.chr << 65.chr << 66.chr << 67.chr << 68.chr << 69.chr << IoConstants::ETB.chr
    end

    it 'should calc the correct checksum value' do
      correct_sum = (31+65+66+67+68+69+23)
      expect(BaseAstmState.calculate_checksum_when_message_is_already_built @message).to eq correct_sum
    end
  end

  context 'checksum on STX >> ETX' do

    before do
      @message = '' << IoConstants::STX.chr << 31.chr << 65.chr << 66.chr << 67.chr << 68.chr << 69.chr << IoConstants::ETX.chr
    end

    it 'should calc the correct checksum value' do
      correct_sum = (31+65+66+67+68+69+3)
      expect(BaseAstmState.calculate_checksum_when_message_is_already_built @message).to eq correct_sum
    end
  end

end