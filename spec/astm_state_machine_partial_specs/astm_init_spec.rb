###+------------------+###***
###+-------INIT-------+###***
###+------------------+###***
  describe 'when initialising a new state machine test' do

    it 'should initialise properly test' do
      astm_state_machine = MrBeam::AstmStateMachine.new 'test1', 'test2', 'test3'

      expect(astm_state_machine.astm_channel_connection).to eq 'test1'
      expect(astm_state_machine.io_channel_delegate).to eq 'test2'
      expect(astm_state_machine.io_service).to eq 'test3'
      expect(astm_state_machine.timeout_in_seconds).to eq nil
      expect(astm_state_machine.tx_buffer).to eq []
      expect(astm_state_machine.rx_buffer).to eq []
      expect(astm_state_machine.channel_busy).to eq false
    end

  end
