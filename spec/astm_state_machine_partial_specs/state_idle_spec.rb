require 'mr_beam/astm_state_machine'

##+------------------+##
##+-------IDLE-------+##
##+------------------+##


describe 'when state machine is running' do
  context 'when current state is Idle' do
    it 'should remain on idle state if there is nothing to do' do

      io_object = double(IO,)
      @sample_message= ''

      @message_counter = 0
      allow(io_object).to receive(:readbyte) do
        to_return = @sample_message[@message_counter]
        @message_counter += 1
        to_return
      end

      #I must create a system that make possible to set ready true after a while
      current_time = Time.now
      allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

      @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil

      @astm_state_machine.channel_busy = false
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      @astm_state_machine.next
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
    end
  end

  context 'enquire received' do
    before do
      io_object = double(IO, read: IoConstants::ENQ)
      allow(io_object).to receive(:readpartial).with(2560) {IoConstants::ENQ}

      #I must create a system that make possible to set ready true after a while
      current_time = Time.now
      allow(io_object).to receive(:ready?) {Time.now < current_time + 1}


      @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
      @astm_state_machine.set_state AstmStates::Idle
      @astm_state_machine.current_read_message = io_object.readpartial(2560)
    end

    it 'should be set current_read_message when something arrives by the IO stream' do
      expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      expect(@astm_state_machine.current_read_message).to eq IoConstants::ENQ
      @astm_state_machine.current_state_instance.send 'message_evaluation_variables_reset'
    end

  end
end