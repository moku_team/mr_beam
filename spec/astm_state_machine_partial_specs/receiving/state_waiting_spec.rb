require 'time'
# 'when state machine has nothing to transmit so works on reading mode'

  describe 'when state machine goes on waiting state' do

    #+------------------+#
    #+------WAITING-----+#
    #+------------------+#


    context 'session timed out' do
      before do

        io_object = double(IO, putc: IoConstants::ACK)
        @sample_message= IoConstants::ENQ.chr

        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = @sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}
        allow(io_object).to receive(:eof?)
        allow(io_object).to receive(:closed?)


        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil


        @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
        @astm_state_machine.current_read_message = @sample_message
        @astm_state_machine.next

      end

      # +----------------------------------------------------------------------------------------------+

      it 'Should be on Waiting state with 30 seconds timeout' do
        @astm_state_machine.set_state AstmStates::ReceiveBranch::Awake
        @astm_state_machine.next
        expect(@astm_state_machine.channel_busy?).to eq true
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Waiting
      end

      # +----------------------------------------------------------------------------------------------+

      it 'should go back to idle after 30 seconds' do
        @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
        @astm_state_machine.current_state_instance.reset_timeout(30, Time.now - 45)
        expect(@astm_state_machine.timeout_in_seconds).to eq 30
        @astm_state_machine.next
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      end
    end


    # +----------------------------------------------------------------------------------------------+
    # +----------------------------------------------------------------------------------------------+
    # +----------------------------------------------------------------------------------------------+

    context 'EOT received' do
      before do

        io_object = double(IO, putc: IoConstants::ACK)
        @sample_message= IoConstants::EOT.chr

        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = @sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}
        allow(io_object).to receive(:eof?)
        allow(io_object).to receive(:closed?)

        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil


        @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
        @astm_state_machine.current_read_message = @sample_message
        @astm_state_machine.next

      end


      # +----------------------------------------------------------------------------------------------+

      it 'should go back to Idle' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      end
    end

    # +----------------------------------------------------------------------------------------------+
    # +----------------------------------------------------------------------------------------------+
    # +----------------------------------------------------------------------------------------------+

    context 'ACK received from AWAKE state' do
      before :each do
        io_object = double(IO, putc: IoConstants::ACK)
        allow(io_object).to receive(:readpartial).with(2560) {IoConstants::ACK.chr}
        allow(io_object).to receive(:eof?)
        allow(io_object).to receive(:closed?)
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        @astm_state_machine.set_state AstmStates::ReceiveBranch::Awake
        @astm_state_machine.current_read_message = io_object.readpartial(2560)
        @astm_state_machine.next
      end

      # +----------------------------------------------------------------------------------------------+

      it 'should be on wait state' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Waiting
      end

      # +----------------------------------------------------------------------------------------------+

      it 'frame number should be equal to 1' do
        expect(@astm_state_machine.frame_number).to eq 1
      end

      # +----------------------------------------------------------------------------------------------+

      it 'timer should be set at 30 second' do
        expect(@astm_state_machine.timeout_in_seconds).to eq 30
      end

      # +----------------------------------------------------------------------------------------------+

      it 'timeout should not be expired' do
        expect(@astm_state_machine.timeout_expired?).to eq false

      end
    end


  # --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- *

  end
