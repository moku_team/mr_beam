require 'time'

require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


# 'when state machine has nothing to transmit so works on reading mode'

  describe 'when state machine goes on frame received state' do

    #+-------------------------+#
    #+------Frame_Received-----+#
    #+-------------------------+#


    context 'well format frame received' do

      before :each do
        #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

        sample_message = File.read('spec/fixtures/astm/correct_frame.binary')
        @payload = File.read('spec/fixtures/astm/correct_frame_payload_only.binary')
        # first_substring = sample_message.split(IoConstants::CR.chr).first

        io_channed_delegate = double(IO, received: true)
        io_object = double(IO, putc: IoConstants::ACK)
        @message_counter = 0
        allow(io_object).to receive(:readbyte) do
          to_return = sample_message[@message_counter]
          @message_counter += 1
          to_return
        end

        #I must create a system that make possible to set ready true after a while
        current_time = Time.now
        allow(io_object).to receive(:ready?) {Time.now < current_time + 1}
        allow(io_object).to receive(:eof?)
        allow(io_object).to receive(:closed?)


        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, nil


        @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
        @astm_state_machine.current_read_message = sample_message
        @astm_state_machine.next
        #second next is needed because the working logic is weapped on next method
        @astm_state_machine.next
      end

      it 'payload should be correct' do
        expect(@astm_state_machine.payload).to eq @payload
      end

      it 'timeout should be set to 30 seconds' do
        expect(@astm_state_machine.timeout_in_seconds).to eq 30
      end

      it 'should message_evaluation_variables_reset reset correct things' do

        @astm_state_machine.set_state AstmStates::ReceiveBranch::FrameReceived
        @astm_state_machine.current_state_instance.send 'reset_frame_variables'

        expect( @astm_state_machine.current_state_instance.stateful.errors_here).to eq false
        expect( @astm_state_machine.current_state_instance.stateful.checksum_counter).to eq 0x0
        expect( @astm_state_machine.current_state_instance.stateful.checksum_values).to eq({cs1: nil, cs2: nil})
        expect( @astm_state_machine.current_state_instance.stateful.payload).to eq ''.force_encoding('binary')


      end


    end









    context 'bad format frame received' do

      describe 'not started with stx' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/without_stx_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first




          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)
          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end

          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}
          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
        end

        it 'should raise MessageDoesntBeginWithStxError' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::MessageDoesntBeginWithStxError)
        end
      end

      describe 'wrong frame number' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/wrong_frame_number_frame.binary')
          io_object = double(IO, putc: IoConstants::ACK)
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
          @astm_state_machine.set_state AstmStates::ReceiveBranch::FrameReceived

          # first_substring = sample_message.split(IoConstants::CR.chr).first

        end

        it 'should raise InvalidFrameNumber' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::InvalidFrameNumber)
        end
      end

      describe 'ETB or ETX is missing' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/without_etb_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first


          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)

          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end


          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next


        end

        it 'should raise MissingEtbOrEtxError' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::MissingEtbOrEtxError)
        end
      end

      describe 'without last cr' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/without_last_cr_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first


          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)

          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end


          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
        end

        it 'should raise MissingLastCRError' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::MissingLastCRError)
        end
      end

      describe 'without last lf' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/without_last_lf_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first


          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)

          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end


          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}
          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
        end

        it 'should raise MissingLastLFError' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::MissingLastLFError)
        end
      end

      #---

      describe 'wrong cs1' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/wrong_cs1_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first


          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)

          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end


          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
        end

        it 'should raise MissingLastLFError' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::WrongCs1Error)
        end
      end



      describe 'wrong cs2' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/wrong_cs2_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first


          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)

          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end


          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
        end

        it 'should raise WrongCs2Error' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::WrongCs2Error)
        end
      end



      describe 'wrong cs1 and cs2' do
        before :each do
          #sample_message = 'H|\\^&|||Becton Dickinson||||||||V1.0|20170105172015\rP|1||55816||Kirtley, Jerrold|||||||||||||||||||||||||||1402\rO|1|102608||^^^PLUSANF^449318919032|R||20161027000000|||||||20161027101728||||||||20161230095702\rR|1|^^^GND^449318919032|INST_NEGATIVE2|||||F|||20161027101728|20161230095657|BACTECFX^93^5^1^B-J01\rL|1|N\r'

          @sample_message = File.read('spec/fixtures/astm/wrong_cs1_and_cs2_frame.binary')
          # first_substring = sample_message.split(IoConstants::CR.chr).first


          io_channed_delegate = double(IO, received: true)
          io_object = double(IO, putc: IoConstants::ACK)

          @message_counter = 0
          allow(io_object).to receive(:readbyte) do
            to_return = @sample_message[@message_counter]
            @message_counter += 1
            to_return
          end


          #I must create a system that make possible to set ready true after a while
          current_time = Time.now
          allow(io_object).to receive(:ready?) {Time.now < current_time + 1}

          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_channed_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_channed_delegate, io_service


          @astm_state_machine.set_state AstmStates::ReceiveBranch::Waiting
          @astm_state_machine.current_read_message = @sample_message
          @astm_state_machine.next
        end

        it 'should raise MissingLastLFError' do
          expect{@astm_state_machine.current_state_instance.send 'process_message', @sample_message}.to raise_error(AstmErrors::WrongCs1Error)
        end
      end
    end

    context 'helper methods testing' do

      it 'is_eot should return true if it receive an eot byte' do
        expect(AstmStates::ReceiveBranch::FrameReceived.is_eot?(IoConstants::EOT)).to eq true
      end

      it 'is_eot should return false if it receive a not eot byte' do
        expect(AstmStates::ReceiveBranch::FrameReceived.is_eot?(IoConstants::ENQ)).to eq false
      end


      it 'is_eot should return true if it receive an cr byte' do
        expect(AstmStates::ReceiveBranch::FrameReceived.is_cr_byte_?(IoConstants::CR)).to eq true
      end

      it 'is_eot should return false if it receive a not cr byte' do
        expect(AstmStates::ReceiveBranch::FrameReceived.is_cr_byte_?(IoConstants::ENQ)).to eq false
      end


      it 'is_eot should return true if it receive an lf byte' do
        expect(AstmStates::ReceiveBranch::FrameReceived.is_lf_byte_?(IoConstants::LF)).to eq true
      end

      it 'is_eot should return false if it receive a not lf byte' do
        expect(AstmStates::ReceiveBranch::FrameReceived.is_lf_byte_?(IoConstants::ENQ)).to eq false
      end


    end


    context 'message processing' do



      describe 'current_message is empty' do
        before do
          io_object = double(IO, putc: IoConstants::NACK)
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil

          @astm_state_machine.set_state AstmStates::ReceiveBranch::FrameReceived
          @astm_state_machine.current_read_message = ''
          @astm_state_machine.next
        end

        it 'should go on waiting state with a 30 seconds timer' do
          expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::FrameReceived
          expect(@astm_state_machine.timeout_in_seconds).to eq 30

        end
      end

      describe 'current_message is well formed' do
        before do
          io_object = double(IO, putc: IoConstants::ACK)
          io_object_for_delegate = double(IO, received: true )
          allow(io_object).to receive(:eof?)
          allow(io_object).to receive(:closed?)

          io_service = MrBeam::AstmIoService.new io_object, io_object_for_delegate, {}, Mutex.new, 'Unpacked'
          @astm_state_machine = MrBeam::AstmStateMachine.new io_object, io_object_for_delegate, io_service
          @astm_state_machine.set_state AstmStates::ReceiveBranch::FrameReceived
          @astm_state_machine.current_read_message = File.read('spec/fixtures/astm/sample_frame_correct_epicenter_export.binary')
          @astm_state_machine.frame_number= 1
          @astm_state_machine.next

        end

        it 'should go on waiting state with a 30 seconds timer' do
          expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Waiting
          expect(@astm_state_machine.timeout_in_seconds).to eq 30
          expect(@astm_state_machine.frame_number.chr).to eq '2'

        end
      end




    end
    

  end
