require 'mr_beam/astm_state_machine'
# 'when state machine has nothing to transmit so works on reading mode'

  describe 'when state machine receives an enquire' do

    #+------------------+#
    #+------AWAKE-------+#
    #+------------------+#
    context 'when channel is free' do
      before do
        # io_object = double(IO)
        io_object = double(IO, putc: IoConstants::ACK)
        # allow(io_object).to receive(:readpartial).with(2560) {IoConstants.ENQ.chr}
        # allow(io_object).to receive(:read) {IoConstants::ENQ}
        #expect(@delegate).to receive(:invoke).with('Example content')
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        allow(@astm_state_machine.current_state_instance).to receive(:read) {IoConstants::ENQ.chr}


        @astm_state_machine.channel_busy = false
        @astm_state_machine.current_read_message = @astm_state_machine.current_state_instance.read
      end

      it 'should go form idle to awake state on equiry' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
        expect(@astm_state_machine.current_state_instance.send 'something_to_send?').to be false
        @astm_state_machine.current_state_instance.next

        expect(@astm_state_machine.current_read_message).to eq IoConstants::ENQ.chr          # *ord* is the antidote if you use *chr*
        # @astm_state_machine_partial_specs.current_state_instance.send 'message_evaluation_variables_reset'
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Awake
      end

      it 'should write ACK and pass to waiting state' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
        expect(@astm_state_machine.current_state_instance.send 'something_to_send?').to be false
        @astm_state_machine.current_state_instance.next
        expect(@astm_state_machine.current_read_message).to eq IoConstants::ENQ.chr          # *ord* is the antidote if you use *chr*
        # @astm_state_machine_partial_specs.current_state_instance.send 'message_evaluation_variables_reset'
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Awake
        expect(@astm_state_machine.astm_channel_connection.putc).to eq IoConstants::ACK
        @astm_state_machine.next
        #if execution comes from awake, framne counter should be eq to 1
        expect(@astm_state_machine.frame_number).to eq 1
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Waiting
      end
    end


    context 'when channel is busy' do
      before do
        io_object = double(IO, putc: IoConstants::NACK)
        @astm_state_machine = MrBeam::AstmStateMachine.new io_object, nil, nil
        allow(@astm_state_machine.current_state_instance).to receive(:read) {IoConstants::ENQ.chr}
        @astm_state_machine.channel_busy = true
        @astm_state_machine.current_read_message = @astm_state_machine.current_state_instance.read
      end

      it 'should send NACK and return on idle state' do
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
        expect(@astm_state_machine.current_state_instance.send 'something_to_send?').to be false
        @astm_state_machine.current_state_instance.next
        expect(@astm_state_machine.current_read_message).to eq IoConstants::ENQ.chr          # *ord* is the antidote if you use *chr*
        # @astm_state_machine_partial_specs.current_state_instance.send 'message_evaluation_variables_reset'
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::ReceiveBranch::Awake
        expect(@astm_state_machine.astm_channel_connection.putc).to eq IoConstants::NACK
        @astm_state_machine.next
        expect(@astm_state_machine.current_state_instance.class).to eq AstmStates::Idle
      end


    end
  end
