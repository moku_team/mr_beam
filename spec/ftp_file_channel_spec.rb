require 'spec_helper'
require 'mr_beam/modules/ftp/ftp_file_channel'
require 'testftpd'


RSpec.describe Ftp::FtpFileChannel, type: :model do
  describe 'when initialising a new channel' do
    before do
      @subject_object = 'subject_object'
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel_hash = {
          subject: @subject_object,
          config: {
              input_folder: 'input',
              output_folder: 'output',
              input_file_name: 'input_file',
              output_file_name: 'output_file',
              should_overwrite_dest: false,
              processing_folder: 'processing',
              server: '127.0.0.1',
              port: 21212,
              username: 'username',
              password: 'password',
              prevent_start: true
          }
      }
      @block = lambda { |a| a }
    end
    it 'should initialise properly' do
      channel = MrBeam::FtpFileChannel.new @channel_hash, @block

      expect(channel.subject).to eq @subject_object
      expect(channel.message_queue).to be_empty
      expect(channel.input_folder).to eq 'input'
      expect(channel.output_folder).to eq 'output'
      expect(channel.input_file_name).to eq 'input_file'
      expect(channel.output_file_name).to eq 'output_file'
      expect(channel.should_overwrite_dest).to be false
      expect(channel.receive_callback_block).to eq @block
      expect(channel.processing_folder).to eq 'processing'
      expect(channel.ftp_data).to_not be_nil
      expect(channel.ftp_data[:server]).to eq '127.0.0.1'
      expect(channel.ftp_data[:port]).to eq 21212
      expect(channel.ftp_data[:username]).to eq 'username'
      expect(channel.ftp_data[:password]).to eq 'password'

      #TODO I might add a check for the Thread to have started, but we'll see
    end

    after do
      @ftp_server.shutdown
    end
  end

  describe 'when processing an input folder with a valid input file' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_folder_processing_testing)

      fixture = 'spec/fixtures/input.txt'
      input_folder =   'spec/support/ftp/input'

      input_file_name = 'input.txt'
      @input_file = "#{input_folder}/#{input_file_name}"

      processing_folder =   'spec/support/ftp/processing'

      @processing_file = "#{processing_folder}/#{input_file_name}"
      FileUtils.copy fixture, @input_file

      @delegate = double('delegate')
      @channel.receive_callback_block = lambda { |msg| @delegate.invoke(msg) }
    end

    it 'should detect the file, delete it and call the callback block' do
      expect(@delegate).to receive(:invoke).with('Example content')
      @channel.process_folder

      expect(Dir[@input_file]).to be_empty
      expect(Dir[@processing_file]).to be_empty
    end

    after do
      FileUtils.rm_f @input_file
      @ftp_server.shutdown
    end
  end

  describe 'when processing an input folder without a valid input file' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_folder_processing_testing)

      fixture = 'spec/fixtures/input.txt'
      input_folder =   'spec/support/ftp/input'

      input_file_name = 'wrong_input.txt'
      @input_file = "#{input_folder}/#{input_file_name}"
      FileUtils.copy fixture, @input_file

      @delegate = double('delegate')
      @channel.receive_callback_block = lambda { |msg| @delegate.invoke(msg) }
    end

    it 'should do nothing' do
      @channel.process_folder

      expect(Dir[@input_file].length).to eq 1
    end

    after do
      FileUtils.rm_f @input_file
      @ftp_server.shutdown
    end
  end

  describe 'when sending a message' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_queue_processing_testing)
      output_folder = 'spec/support/ftp/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"
      @delegate = double('delegate')
      @example_message = {
          message: 'Example message',
          block: lambda { |outcome| @delegate.invoke(outcome) }
      }
    end

    it 'should write the file in the right folder' do
      expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)
      @channel.send(:send_message, @example_message)

      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Example message'
    end

    after do
      FileUtils.rm_f @output_file
      @ftp_server.shutdown
    end
  end

  describe 'when switching to another folder' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_folder_processing_testing)
    end

    it 'should switch to that folder' do
      @channel.send(:switch_to, 'input')
      expect(@channel.ftp_connection.pwd).to include 'input'
    end

    after do
      @ftp_server.shutdown
    end
  end

  describe 'when processing a non-empty message queue with the overwrite option = true' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_queue_processing_testing_overwrite)
      output_folder = 'spec/support/ftp/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"

      @delegate = double('delegate')
      @channel.message_queue << {
          message: 'Oldest example message',
          block: lambda { |outcome| @delegate.invoke(outcome) }
      }
      @channel.message_queue << {
          message: 'More recent example message',
          block: nil
      }
      @channel.message_queue << {
          message: 'Even more recent example message',
          block: nil
      }

      fixture = 'spec/fixtures/input.txt'
      FileUtils.copy fixture, @output_file
    end

    it 'should extract the oldest message and write it as a file in the correct folder, no matter if there is a file' do
      expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)
      @channel.send(:process_queue)
      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Oldest example message'
      expect(@channel.message_queue.length).to eq 2
    end

    after do
      FileUtils.rm_f @output_file
      @ftp_server.shutdown
    end
  end

  describe 'when processing a non-empty message queue with the overwrite option = false' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_queue_processing_testing_no_overwrite)
      output_folder = 'spec/support/ftp/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"

      @delegate = double('delegate')
      @channel.message_queue << {
          message: 'Oldest example message',
          block: lambda { |outcome| @delegate.invoke(outcome) }
      }
      @channel.message_queue << {
          message: 'More recent example message',
          block: nil
      }
      @channel.message_queue << {
          message: 'Even more recent example message',
          block: nil
      }

      fixture = 'spec/fixtures/input.txt'
      FileUtils.copy fixture, @output_file
    end

    it 'should do nothing if there\'s a file already there' do
      @channel.send(:process_queue)
      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Example content'
      expect(@channel.message_queue.length).to eq 3
    end

    it 'should extract the oldest message and write it as a file in the correct folder if there\'s no file already there' do
      expect(@delegate).to receive(:invoke).with(a_successful_transmit_outcome)
      FileUtils.rm_f @output_file
      @channel.send(:process_queue)
      expect(Dir[@output_file].length).to eq 1
      expect(File.open(@output_file, 'rb') {|f| f.read}).to eq 'Oldest example message'
      expect(@channel.message_queue.length).to eq 2
    end

    after do
      FileUtils.rm_f @output_file
      @ftp_server.shutdown
    end
  end

  describe 'when processing an empty message queue' do
    before do
      @ftp_server = TestFtpd::Server.new(port: 21212, root_dir: 'spec/support/ftp')
      @ftp_server.start

      @channel = FactoryGirl.build(:ftp_file_channel_for_queue_processing_testing)
      output_folder = 'spec/support/ftp/output'
      output_file_name = 'output.txt'
      @output_file = "#{output_folder}/#{output_file_name}"
    end

    it 'should do nothing' do
      @channel.send(:process_queue)

      expect(@channel.message_queue).to be_empty
      expect(Dir[@output_file]).to be_empty
    end

    after do
      FileUtils.rm_f @output_file
    end
  end
end
