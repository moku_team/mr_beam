require 'spec_helper'
require 'mr_beam/channel'

RSpec.describe MrBeam::Channel, type: :model do
  describe 'when initialising a new channel' do
    before do
      @subject_object = 'subject_object'
      @channel_hash = {
          subject: @subject_object
      }
      @block = lambda { |a| a }
    end
    it 'should initialise properly' do
      channel = MrBeam::Channel.new @channel_hash, @block

      expect(channel.subject).to eq @subject_object
      expect(channel.receive_callback_block).to eq @block
    end
  end

  describe 'when checking if the channel has a subject' do
    before do
      @subject_object = 'subject_object'
      @wrong_subject_object = 'wrong_subject_object'

      @channel = FactoryGirl.build(:channel_with_example_subject)
    end

    it 'should return true if the subject matches' do
      expect(@channel.has_subject?(@subject_object)).to be true
    end

    it 'should return false if the subject does not match' do
      expect(@channel.has_subject?(@wrong_subject_object)).to be false
    end
  end
end