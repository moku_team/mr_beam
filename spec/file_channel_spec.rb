require 'spec_helper'
require 'mr_beam/file_channel'

RSpec.describe MrBeam::FileChannel, type: :model do
  describe 'when initialising a new file channel' do
    before do
      @subject_object = 'subject_object'
      @channel_hash = {
          subject: @subject_object,
          config: {
              input_folder: 'input',
              output_folder: 'output',
              input_file_name: 'input_file',
              output_file_name: 'output_file',
              should_overwrite_dest: false
          }
      }
      @block = lambda { |a| a }
    end
    it 'should initialise properly' do
      channel = MrBeam::FileChannel.new @channel_hash, @block

      expect(channel.subject).to eq @subject_object
      expect(channel.message_queue).to be_empty
      expect(channel.input_folder).to eq 'input'
      expect(channel.output_folder).to eq 'output'
      expect(channel.input_file_name).to eq 'input_file'
      expect(channel.output_file_name).to eq 'output_file'
      expect(channel.should_overwrite_dest).to be false
      expect(channel.receive_callback_block).to eq @block
    end
  end

  describe 'when enqueuing some messages' do
    before do
      @channel = FactoryGirl.build(:empty_file_channel)
      @msg1 = 'msg1'
      @msg2 = 'msg2'
      @msg3 = 'msg3'
    end
    it 'should enqueue them properly' do
      @channel.enqueue_message @msg1, lambda { puts 'a' }
      @channel.enqueue_message @msg3, nil
      @channel.enqueue_message @msg2, lambda { puts 'a' }

      expect(@channel.message_queue.length).to eq 3
      expect(@channel.message_queue[0][:message]).to eq @msg1
      expect(@channel.message_queue[0][:block]).to_not be_nil
      expect(@channel.message_queue[1][:message]).to eq @msg3
      expect(@channel.message_queue[1][:block]).to be_nil
      expect(@channel.message_queue[2][:message]).to eq @msg2
      expect(@channel.message_queue[2][:block]).to_not be_nil
    end
  end

  describe 'when transmitting some messages' do
    before do
      @channel = FactoryGirl.build(:empty_file_channel)
      @msg1 = 'msg1'
      @msg2 = 'msg2'
      @msg3 = 'msg3'
    end
    it 'should enqueue them properly' do
      @channel.enqueue_message @msg1, lambda { puts 'a' }
      @channel.enqueue_message @msg3, nil
      @channel.enqueue_message @msg2, lambda { puts 'a' }

      expect(@channel.message_queue.length).to eq 3
      expect(@channel.message_queue[0][:message]).to eq @msg1
      expect(@channel.message_queue[0][:block]).to_not be_nil
      expect(@channel.message_queue[1][:message]).to eq @msg3
      expect(@channel.message_queue[1][:block]).to be_nil
      expect(@channel.message_queue[2][:message]).to eq @msg2
      expect(@channel.message_queue[2][:block]).to_not be_nil
    end
  end
end