require 'mr_beam/custom_errors'

describe 'custom errors testing' do

  context 'astm_errors' do

    it 'invalidFrameNumber should have correct text' do
      error = AstmErrors::InvalidFrameNumber.new
      error.to_s == 'unable to complete operation because the frame number doesn\'t be in 0-7 interval'
    end

    it 'MessageDoesntBeginWithStxError should have correct text' do
      error = AstmErrors::MessageDoesntBeginWithStxError.new
      error.to_s == 'unable to complete operation because the message doesn\'t begin with the STX character'
    end

    it 'MissingEtbOrEtxError should have correct text' do
      error = AstmErrors::MissingEtbOrEtxError.new
      error.to_s == 'unable to complete operation because ETB - End of Transmission Block - or ETX - End of Transmission - is missing on the message'
    end

    it 'MissingLastCRError should have correct text' do
      error = AstmErrors::MissingLastCRError.new
      error.to_s =='unable to complete operation because last CR is missing on the message'
    end

    it 'MissingLastLFError should have correct text' do
      error = AstmErrors::MissingLastLFError.new
      error.to_s == 'unable to complete operation because last LF is missing on the message'
    end

    it 'WrongCs1Error should have correct text' do
      error = AstmErrors::WrongCs1Error.new
      error.to_s == 'unable to complete operation cause of wrong cs1'
    end

    it 'WrongCs2Error should have correct text' do
      error = AstmErrors::WrongCs2Error.new
      error.to_s == 'unable to complete operation cause of wrong cs2'
    end

  end
end
