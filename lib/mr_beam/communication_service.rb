require 'net/ftp'
require 'mr_beam/modules/shared_folders/shared_folders_file_channel'
require 'mr_beam/modules/ftp/ftp_file_channel'
require 'mr_beam/modules/astm_socket/astm_single_socket_channel'
require 'mr_beam/modules/astm_socket/astm_double_socket_channel'
require 'mr_beam/modules/mllp_socket/mllp_single_socket_channel'
require 'mr_beam/modules/mllp_socket/mllp_double_socket_channel'
require 'mr_beam/modules/serial_emulation_socket/serial_emulation_single_socket_channel'
require 'mr_beam/modules/serial_emulation_socket/serial_emulation_double_socket_channel'
require 'mr_beam/modules/frend_serial_socket/frend_serial_single_socket_channel'
require 'mr_beam/modules/frend_serial_socket/frend_serial_double_socket_channel'
require 'mr_beam/modules/alifax_mt6500_socket/alifax_mt6500_single_socket_channel' #only single duplex channel
require 'singleton'
require 'celluloid'

class MrBeam::CommunicationService
  include Singleton

  attr_accessor :channels
  attr_reader :logger

  def initialize
    @channels = []

    Celluloid.boot
  end

  def add_channel(channel_config, &block)
    config = channel_config[:config]
    case channel_config[:type]
    when 'ftp'
      @channels << Ftp::FtpFileChannel.new(channel_config, block)
    when 'shared_folders'
      @channels << SharedFolders::SharedFoldersFileChannel.new(channel_config, block)
    when 'socket'
      case channel_config[:protocol_config][:protocol_name]
      when 'astm'
        if config.try(:[], :sender) && config.try(:[], :receiver)
          @channels << AstmSocket::AstmDoubleSocketChannel.new(channel_config, block)
        else
          @channels << AstmSocket::AstmSingleSocketChannel.new(channel_config, block)
        end
      when 'hl7'
        if config.try(:[], :sender) && config.try(:[], :receiver)
          @channels << MllpSocket::MllpDoubleSocketChannel.new(channel_config, block)
        else
          @channels << MllpSocket::MllpSingleSocketChannel.new(channel_config, block)
        end
      when 'serial_emulation'
        if config.try(:[], :sender) && config.try(:[], :receiver)
          @channels << SerialEmulationSocket::SerialEmulationDoubleSocketChannel.new(channel_config, block)
        else
          @channels << SerialEmulationSocket::SerialEmulationSingleSocketChannel.new(channel_config, block)
        end
      when 'frend_serial'
        if config.try(:[], :sender) && config.try(:[], :receiver)
          @channels << FrendSerialSocket::FrendSerialDoubleSocketChannel.new(channel_config, block)
        else
          @channels << FrendSerialSocket::FrendSerialSingleSocketChannel.new(channel_config, block)
        end
      when 'alifax_serial'
        @channels << AlifaxMt6500Socket::AlifaxMt6500SingleSocketChannel.new(channel_config, block)
      end
    else
      raise ChannelErrors::ChannelConfigurationError
    end
  end

  def transmit(message, subject, &block)
    channel_to_transmit = @channels.select { |c| c.has_subject?(subject) }

    # The channel should be one, but I still iterate through the returned array
    channel_to_transmit.each do |channel|
      channel.transmit message, block
    end
  end

  def exception_handler &block
    Celluloid.exception_handler { |ex|
      block.call(ex) unless internal_errors.include? ex.class
    }
  end

  def stop_all timeout = 60
    # Fire a stop command on every channel
    @channels.each do |channel|
      channel.stop!
    end

    # Wait for all the channels to shut down
    all_stopped = @channels.length == 0 # If there are no channels, all_stopped should immediately be true
    time_passed = 0

    until all_stopped || time_passed > timeout do
      sleep 1
      all_stopped = @channels.map(&:stopped?).reduce(:&)
      time_passed += 1
    end

    non_stopping_channels = @channels.select{|c| !c.stopped?}
    raise ChannelErrors::ChannelStopTimeOut.new('Channel stop unsuccessful', non_stopping_channels) unless all_stopped

    Celluloid.shutdown
    true
  end

  def channel_status(subject = nil)
    if subject
      channel = @channels.select {|c| c.has_subject?(subject) }
      channel.length > 0 ? channel[0].get_connection_status : nil
    else
      @channels.map { |c| { subject: c.subject, status: c.get_connection_status } }
    end
  end

  private

  # These errors won't be fired by MrBeam
  def internal_errors
    [ChannelErrors::IdleTimerExpired, Timeout::Error]
  end
end
