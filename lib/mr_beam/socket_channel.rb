require 'mr_beam/channel'

class MrBeam::SocketChannel < MrBeam::Channel
  using MrBeam::ObjectRefinements
  attr_accessor :tx_buffer
  attr_accessor :tx_buffer_mutex
  attr_accessor :idle_timer

  def initialize(hash, block)
    super hash, block

    @tx_buffer = []
    @tx_buffer_mutex = Mutex.new
    @idle_timer = (hash[:config][:idle_timer].present? && hash[:config][:idle_timer].to_f > 0) ? hash[:config][:idle_timer].to_f : nil
  end

  def transmit message, block
    @tx_buffer_mutex.synchronize do
      @tx_buffer << {
          message: message,
          block: block
      }
    end
  end
end