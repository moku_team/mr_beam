class MrBeam::TransmitOutcome
  attr_accessor :errors

  def initialize(hash = nil)
    if hash && hash[:errors]
      @errors = hash[:errors]
    else
      @errors = []
    end
  end

  def success?
    not(@errors && @errors.length > 0)
  end
end