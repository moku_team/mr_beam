require 'socket'

class MrBeam::ConnectionService
  def self.create_socket(config_hash)
    socket = nil
    if config_hash[:mode] == 'server'
      server = TCPServer.new config_hash[:port]
      begin
        Timeout::timeout(120) do
          socket = server.accept
          socket.timeout = IoConstants::READ_TIMEOUT
        end
      ensure
        server.close
      end
    else
      begin
        socket = TCPSocket.new config_hash[:address], config_hash[:port]
        socket.timeout = IoConstants::READ_TIMEOUT
      rescue Errno::ECONNREFUSED
        sleep 5
        retry
      end
    end

    socket
  end

  def self.create_ftp_connection(config_hash)
    ftp_data = {
        server:   config_hash[:server],
        port:     config_hash[:port],
        username: config_hash[:username],
        password: config_hash[:password],
        passive:  config_hash[:passive]  # true or false
    }

    begin
      ftp_connection = Net::FTP.new
      ftp_connection.connect ftp_data[:server], ftp_data[:port]
      ftp_connection.login ftp_data[:username], ftp_data[:password]
      if config_hash[:passive].present?
        ftp_connection.passive = config_hash[:passive]  # default true
      end
      return ftp_connection

    rescue Errno::ECONNREFUSED, Net::FTPTempError => err
      @logger.warn("FTP is down due to #{err.message}")

      ftp_connection.try :close
      sleep 5
      retry
    end

  end
end
