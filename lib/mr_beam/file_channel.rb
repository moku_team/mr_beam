# Abstract class
class MrBeam::FileChannel < MrBeam::Channel
  attr_accessor :message_queue, :input_folder, :output_folder, :failed_folder, :input_file_name, :output_file_name, :should_overwrite_dest, :poll_interval, :input_persistence_folder

  def initialize(hash, block)
    super hash, block
    config = hash[:config]
    @input_folder = config[:input_folder]
    @output_folder = config[:output_folder]
    @failed_folder = config[:failed_folder]
    @input_file_name = config[:input_file_name]
    @output_file_name = config[:output_file_name]
    @should_overwrite_dest = config[:should_overwrite_dest]
    @input_persistence_folder = config[:input_persistence_folder]
    @should_overwrite_dest = config[:should_overwrite_dest]
    @poll_interval = config[:poll_interval] || 1
    @message_queue = []
  end

  def transmit(message, block)
    enqueue_message message, block
  end

  def enqueue_message(message, block)
    msg_hash = {
        message: message,
        block: block
    }
    @message_queue << msg_hash
  end
end
