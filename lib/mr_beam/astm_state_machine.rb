require 'mr_beam/object_refinements'
require 'state_pattern'

#INCLUSIONS HERE
require 'mr_beam/modules/astm_states/base_astm_state'
require 'mr_beam/modules/astm_states/astm_states'
require 'mr_beam/modules/astm_states/astm_states_idle'
require 'mr_beam/modules/astm_states/astm_states_stopping'
require 'mr_beam/custom_errors'

#ReceiveBranch
require 'mr_beam/modules/astm_states/receive_branch/astm_states_awake'
require 'mr_beam/modules/astm_states/receive_branch/astm_states_waiting'
require 'mr_beam/modules/astm_states/receive_branch/astm_states_frame_received'

#SendBranch
require 'mr_beam/modules/astm_states/send_branch/astm_states_data_to_send'
require 'mr_beam/modules/astm_states/send_branch/astm_states_waiting'
require 'mr_beam/modules/astm_states/send_branch/astm_states_next_frame_set_up'
require 'mr_beam/modules/astm_states/send_branch/astm_states_frame_ready'
require 'mr_beam/modules/astm_states/send_branch/astm_states_old_frame_set_up'
require 'mr_beam/modules/astm_states/send_branch/astm_states_waiting_write_response'



class MrBeam::AstmStateMachine
  using MrBeam::ObjectRefinements
  include StatePattern

  attr_accessor   :astm_channel_connection
  attr_accessor   :actor_delegate

  attr_accessor   :channel_busy
  attr_accessor   :checksum_counter
  attr_accessor   :checksum_values
  attr_accessor   :current_read_message
  attr_accessor   :errors_here
  attr_accessor   :execution_begins_at
  attr_accessor   :frame_number
  attr_accessor   :rx_buffer
  attr_accessor   :timeout_in_seconds
  attr_accessor   :tx_buffer
  attr_accessor   :payload

  attr_accessor   :sending_retries_count
  attr_accessor   :sending_checksum_counter
  attr_accessor   :current_array_of_frame_to_send
  attr_accessor   :current_sending_message_raw_messages_length
  attr_accessor   :current_frame_to_send
  attr_accessor   :entered_on_idle_timestamp

  set_initial_state ::AstmStates::Idle



  def initialize io_connection, actor_delegate
    @astm_channel_connection =  io_connection
    @actor_delegate = actor_delegate

    @tx_buffer = []

    @channel_busy = false
    @errors_here = false
    @checksum_counter = 0x0
    @checksum_values = {cs1: nil, cs2: nil}
    @frame_number = -1
    @payload = ''.force_encoding('binary')
    @timeout_in_seconds = nil

    @sending_retries_count = 0
    @sending_checksum_counter = 0x0
    @current_array_of_frame_to_send = []
    @current_frame_to_send = nil

    @entered_on_idle_timestamp = nil
  end


  def channel_busy?
    self.channel_busy
  end

  def loop_machine
    # :nocov:
    loop do
      sleep 0.02
      if @entered_on_idle_timestamp.present? && @entered_on_idle_timestamp < Time.now - 4.seconds
        #actor_delegate.logger.debug('Long idle, sleeping 1s')
        sleep 1
      end
      begin
        self.next
      rescue ChannelErrors::SoftKillError => e_
        break
      end
    end
    # :nocov
  end

  def timeout_expired?
    (@timeout_in_seconds.present? && @execution_begins_at.present?) ? @timeout_in_seconds < Time.now - @execution_begins_at : false
  end
end
