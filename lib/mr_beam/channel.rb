require 'fileutils'

# Abstract class
  class MrBeam::Channel
    attr_accessor :subject, :receive_callback_block, :channel_status
    attr_reader   :configuration_hash

    def initialize(hash, block)
      @subject = hash[:subject]
      @receive_callback_block = block
      @configuration_hash = hash

      hash[:config][:log_level] = hash[:config][:log_level].to_sym rescue :info

      FileUtils.mkdir_p 'log/mr_beam_log'  unless File.directory? 'log/mr_beam_log'

      @channel_status = :active # available statuses: :active, :stopping, :stopped

      @connection_status = {
          inbound: :initializing,
          outbound: :initializing
      }

      @actor_inactive = {}

      @connection_status_mutex = Mutex.new
      @actor_inactive_mutex = Mutex.new
    end

    def has_subject?(subject)
      @subject == subject
    end

    def get_connection_status
      @connection_status_mutex.synchronize do
        return @connection_status
      end
    end

    def set_connection_status(key, value)
      @connection_status_mutex.synchronize do
        @connection_status[key] = value
      end
    end

    def get_actor_inactive
      @actor_inactive_mutex.synchronize do
        return @actor_inactive
      end
    end

    def set_actor_inactive(key, value)
      @actor_inactive_mutex.synchronize do
        @actor_inactive[key] = value
      end
    end

    def execute_receive_callback(message)
      @receive_callback_block.call(message)
    end

    # Command a channel stop
    def stop!
      @channel_status = :stopping

      if actors_inactive?
        @channel_status = :stopped
      end
    end

    # Check whether the channel has stopped or not
    def stopped?
      @channel_status == :stopped
    end

    # Check whether the channel is going to be stopped or not
    def stopping?
      @channel_status == :stopping
    end

    def signal_actor_stopped(actor_name)
      set_actor_inactive(actor_name, true)

      if actors_inactive? && !stopped?
        @channel_status = :stopped
      end
    end

    # Abstract methods

    def transmit(message, block)
    end

    def actors_inactive?
      actor_names = []
      @supervision_config.each {|instance| actor_names << instance.configuration[:as]  }

      actor_names.map { |name| @actor_inactive[name] }.reduce(:&)
    end

    def send_message
    end
  end
