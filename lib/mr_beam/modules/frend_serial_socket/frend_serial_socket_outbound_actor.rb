require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_actor'

module FrendSerialSocket
  class FrendSerialSocketOutboundActor < FrendSerialSocket::FrendSerialSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :outbound
      @logger = MrBeam::ActorLogger.new channel.subject, :outbound, log_level

      super actor_name, channel, config_hash
    end

    def rx
    end
  end
end