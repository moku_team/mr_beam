module FrendSerialSocket
module FrendSerialSocketConstants
  #FREND CONSTANTS
  STX  = 36
  ETX  = 127

  #MSG TYPES
  ENQ  = 0x40
  TST  = 0x20 #0x14 # o 48, chi lo sa
  QC   = 0x32 # o 80, chi lo sa

  #STATES
  SUCCESS = 1
  FAIL = 255

end
end
