require 'mr_beam/socket_channel'

module FrendSerialSocket
  class FrendSerialSocketChannel < MrBeam::SocketChannel

    def initialize(hash, block)
      super hash, block
    end
  end
end