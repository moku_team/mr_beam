require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_actor'
require 'mr_beam/connection_service'

module FrendSerialSocket
  class FrendSerialSocketInboundActor < FrendSerialSocket::FrendSerialSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :inbound
      @logger = MrBeam::ActorLogger.new channel.subject, :inbound, log_level

      super actor_name, channel, config_hash
    end
  end
end