require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_channel'
require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_duplex_actor'

module FrendSerialSocket
  class FrendSerialSingleSocketChannel < FrendSerialSocket::FrendSerialSocketChannel
    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: FrendSerialSocket::FrendSerialSocketDuplexActor,
                  as: "#{@subject}_actor",
                  args: ["#{@subject}_actor", self, config, config[:log_level]]
              }
          ]
      )
      @supervision_config.deploy
    end
  end
end