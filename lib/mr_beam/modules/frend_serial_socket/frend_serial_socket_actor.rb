require 'mr_beam/channel_actor'
require 'mr_beam/connection_service'
require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_constants'

module FrendSerialSocket
  class FrendSerialSocketActor < MrBeam::ChannelActor
    attr_accessor :role

    def initialize(actor_name, channel, config_hash)
      @current_message = ''
      @config_hash = config_hash
      @stx = FrendSerialSocketConstants::STX
      @etx = FrendSerialSocketConstants::ETX

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')
      @socket = MrBeam::ConnectionService.create_socket(@config_hash)
      @logger.info('Socket created')

      if @role == :duplex || @role == :inbound
        @channel.set_connection_status(:inbound, :connecting)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      end

      if @role == :duplex || @role == :outbound
        @channel.set_connection_status(:outbound, :connecting)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      end

      @channel.set_actor_inactive(@actor_name, false)

      duplex_worker

      stop_actor
    end

    def duplex_worker
      while true
        break if @channel.stopping?

        idle_timer = @channel.idle_timer
        if idle_timer.present? && @idle_started.present? && (Time.now - @idle_started) >= idle_timer.minutes
          raise ChannelErrors::IdleTimerExpired
        end

        if @config_hash[:priority_owner_branch] != 'sending'
          # Receiving priority (default). If both endpoint are sending, mr_beam will fail (non-ack response) and in the next loop mr_beam will grant priority to instrument (receiving).
          unless rx
            tx
            sleep 1
          end
        else
          # Sending priority. Notice: untested mode and uncommon (instrument typically have sending priority).
          sleep 1 while tx
          rx
        end
      end
    end

    def read_message timeout_in_seconds
      transmission_begin_at = Time.now

      read_msg = []
      msg_started = false

      if @socket.ready?
        begin
          while (byte_read_ = @socket.readbyte) && (transmission_begin_at + timeout_in_seconds > Time.now)
            current_read_byte_ = byte_read_
            @logger.trace('%02X' % byte_read_)
            next unless msg_started || byte_read_ == @stx

            @logger.trace('Message started') unless msg_started
            msg_started = true
            read_msg << byte_read_
            if current_read_byte_ == @etx
              @logger.trace('Message finished')
              break
            end
          end
        rescue IO::TimeoutError
          @logger.warn('Timeout receiving the message')

          # Reset the message to avoid processing an incomplete message (and returning ACK for that! since there is no
          # checksum validation right now!).
          read_msg = []
        end
      end
      msg_started && read_msg.present? ? read_msg : nil
    end

    def current_time
      Time.now.strftime('%Y%m%d%H%M%S')
    end

    def rx
      message_ = read_message 60

      return nil if message_.blank?

      @idle_started = Time.now
      @logger.info("Message received: #{message_}")
      @channel.set_connection_status(:inbound, :connected)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      message_type = message_type(message_)
      if checksum_correct(message_)
        case message_type
        when FrendSerialSocketConstants::ENQ
          send_ack(message_type)
        when FrendSerialSocketConstants::TST
          send_ack(message_type)
          @channel.execute_receive_callback unwrap_message(message_).pack('c*')
          @logger.debug('Receive callback called')
        when FrendSerialSocketConstants::QC
          send_ack(message_type)
          # IGNORE FOR NOW
          # @channel.execute_receive_callback message_
        end
      else
        send_nak(message_type)
      end

      true
    end

    def unwrap_message(message)
      message[4..-3]
    end

    def wrap_message(message)
      checksum = checksum(message)
      [FrendSerialSocketConstants::STX].concat(message.concat([checksum, FrendSerialSocketConstants::ETX]))
    end

    def message_type(message)
      message[1]
    end

    def checksum_correct(_message)
      true # LOL, NOPE!
      # message[-2] == checksum(unwrap_message(message))
    end

    def checksum(message)
      message.reduce(&:+) % 0xFF
    end

    def send_ack(message_type)
      packet = [
        message_type,
        00,
        01,
        01
      ]
      packet = wrap_message(packet)
      @channel.tx_buffer << packet
      tx
    end

    def send_nak(message_type)
      packet = [
        message_type,
        00,
        01,
        0xFF
      ]
      packet = wrap_message(packet)
      @channel.tx_buffer << packet
      tx
    end

    def tx
      sending_success = send_message
      return false unless sending_success

      # Always return true. Following `tx` call will return false if tx_buffer is empty.
      true
    end

    def send_message
      return false if @channel.tx_buffer.blank?

      message = @channel.tx_buffer.pop
      @socket.write message.pack('C*')
    end

    def finalise_actor
      @logger.warn('Actor died')

      if @role == :duplex || @role == :inbound
        @channel.set_connection_status(:inbound, :error)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      end

      if @role == :duplex || @role == :outbound
        @channel.set_connection_status(:outbound, :error)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      end
    ensure
      begin
        @socket.try :shutdown, Socket::SHUT_RDWR
      rescue Errno::ENOTCONN
        @logger.warn('The socket was already closed')
      end
      @socket.try :close
    end
  end
end
