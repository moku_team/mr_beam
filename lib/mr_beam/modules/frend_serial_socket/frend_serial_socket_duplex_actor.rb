require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_actor'
require 'mr_beam/connection_service'

module FrendSerialSocket
  class FrendSerialSocketDuplexActor < FrendSerialSocket::FrendSerialSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :duplex
      @logger = MrBeam::ActorLogger.new channel.subject, :duplex, log_level

      super actor_name, channel, config_hash
    end
  end
end