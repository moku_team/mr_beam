require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_channel'
require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_inbound_actor'
require 'mr_beam/modules/frend_serial_socket/frend_serial_socket_outbound_actor'

module FrendSerialSocket
  class FrendSerialDoubleSocketChannel < FrendSerialSocket::FrendSerialSocketChannel
    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: FrendSerialSocket::FrendSerialSocketInboundActor,
                  as: "#{@subject}_inbound_actor",
                  args: ["#{@subject}_inbound_actor", self, config[:receiver], config[:log_level]]
              },
              {
                  type: FrendSerialSocket::FrendSerialSocketOutboundActor,
                  as: "#{@subject}_outbound_actor",
                  args: ["#{@subject}_outbound_actor", self, config[:sender], config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
