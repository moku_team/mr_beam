require 'mr_beam/channel_actor'
require 'mr_beam/connection_service'

module Ftp
  class FtpDuplexActor < MrBeam::ChannelActor
    def initialize(actor_name, channel, config_hash, log_level)
      @logger = MrBeam::ActorLogger.new channel.subject, :duplex, log_level
      @config_hash = config_hash

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')

      @ftp_connection = MrBeam::ConnectionService.create_ftp_connection @config_hash
      @logger.info('FTP connected')

      @channel.set_connection_status(:inbound, :connecting)
      @channel.set_connection_status(:outbound, :connecting)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")

      @channel.set_actor_inactive(@actor_name, false)

      begin
        loop do
          break if @channel.stopping?

          if @ftp_connection.closed?
            @logger.warn('FTP connection closed')
            raise 'FTP connection closed'
          end

          process_folder
          process_queue

          sleep @channel.poll_interval
        end
      ensure
        @ftp_connection.close
      end

      stop_actor
    end

    def process_folder
      switch_to @channel.input_folder

      file_list = []
      begin
        input_regexp = transpile_glob_to_regexp @channel.input_file_name

        file_list = @ftp_connection.nlst.select do |file_name|
          file_name != '.' && file_name != '..' && input_regexp.match(file_name).present?
        end
      rescue Net::FTPTempError => e
        @logger.warn("Error during FTP connection: #{e.inspect}")
      end
      file_list.each do |file|
        destination = "#{@channel.processing_folder}/#{file}"
        begin
          FileUtils.mkdir_p @channel.processing_folder unless Dir.exist? @channel.processing_folder

          @ftp_connection.getbinaryfile(file, destination)
          file_content = File.open(destination, 'rb') { |f| f.read }

          @logger.info("File received: #{file}\n#{file_content}")

          @channel.execute_receive_callback file_content

          @ftp_connection.delete(file) rescue nil #if the ftp server is configured to autodelete files after download, this will raise an error. So I have to rescue it
          FileUtils.rm_f destination
        rescue Errno::EEXIST, Errno::EBUSY, Errno::ENOENT => e
          @logger.warn("Error during reading: #{e.inspect}")
        end
      end
    end

    def switch_to(folder)
      @ftp_connection.chdir("/#{folder}") if @ftp_connection.pwd != folder
    end

    def process_queue
      return if @channel.message_queue.empty?

      switch_to @channel.output_folder

      if @channel.should_overwrite_dest or @ftp_connection.dir(@channel.output_file_name).empty?
        next_message = @channel.message_queue.shift

        send_message next_message
      end
    end

    def send_message(msg_hash)
      message_text = msg_hash[:message]
      transmit_callback = msg_hash[:block]
      outcome = MrBeam::TransmitOutcome.new

      begin
        switch_to @channel.output_folder
        output_file = StringIO.new(message_text)

        output_file_name_ = replace_wildcards(@channel.output_file_name)
        @ftp_connection.delete @channel.output_file_name unless @ftp_connection.dir(@channel.output_file_name).empty?
        @ftp_connection.storbinary("STOR #{output_file_name_}", output_file, 512)
        written_bytes = @ftp_connection.size output_file_name_
      rescue StandardError => e
        raise e if transmit_callback.nil?

        outcome.errors << e.inspect
      end

      unless transmit_callback.nil?
        if outcome.success? && written_bytes != message_text.bytesize
          outcome.errors << 'The written bytes don\'t match the message byte size'
        end

        transmit_callback.call(outcome)
      end
    end

    private

    def replace_wildcards(file_name)
      time_ = Time.now
      file_name_to_return_ = file_name
      begin
        file_name_to_return_ = file_name.gsub('[TIMESTAMP]',                           time_.strftime('%Y%m%d%H%M%S%L'))
                                        .gsub('[TIMESTAMP_TILL_SECONDS]',              time_.strftime('%Y%m%d%H%M%S'))
                                        .gsub('[TIMESTAMP_TILL_SECONDS_SEPARATED]',    time_.strftime('%Y%m%d_%H%M%S'))
                                        .gsub('[TIMESTAMP_2_DIGITS_FRACTION]',         time_.strftime('%2N'))
      rescue StandardError
        # do nothing
      end
      file_name_to_return_
    end

    def transpile_glob_to_regexp(glob_pattern)
      regexp_string = '^' +
                      Regexp.escape(glob_pattern)
                            .gsub('\\*', '[^/]*?')
                            .gsub('\\?', '[^/]') +
                      '$'

      Regexp.new regexp_string
    end
  end
end
