require 'mr_beam/object_refinements'
require 'mr_beam/file_channel'
require 'mr_beam/modules/ftp/ftp_duplex_actor'
require 'celluloid'

module Ftp
  class FtpFileChannel < MrBeam::FileChannel
    attr_reader :processing_folder

    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @processing_folder = config[:processing_folder]

      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: Ftp::FtpDuplexActor,
                  as: "#{@subject}_duplex_actor",
                  args: ["#{@subject}_duplex_actor", self, config, config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end