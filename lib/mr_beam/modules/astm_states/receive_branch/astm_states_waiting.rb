require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'

#Inclusions
require 'mr_beam/modules/astm_states/astm_states'


class AstmStates::ReceiveBranch::Waiting < BaseAstmState
  #https://ruby-doc.org/stdlib-1.9.3/libdoc/timeout/rdoc/Timeout.html
  using MrBeam::ObjectRefinements

  def next
    read

    if stateful.current_read_message == IoConstants::EOT.chr || stateful.timeout_expired?
      stateful.actor_delegate.logger.debug('Timeout expired or EOT has been read in waiting state')
      stateful.channel_busy = false
      transition_to AstmStates::Idle
    elsif stateful.current_read_message.present?
      stateful.actor_delegate.logger.debug('New frame has been read, going to frame received state')
      transition_to AstmStates::ReceiveBranch::FrameReceived
    else
      # :nocov:
      # :nocov:
    end
  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on Waiting State')
    reset_timeout(30)
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from Waiting State')
    # :nocov:
  end
end
