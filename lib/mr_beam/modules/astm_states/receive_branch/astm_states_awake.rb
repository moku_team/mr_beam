require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'



  class AstmStates::ReceiveBranch::Awake < BaseAstmState
    using MrBeam::ObjectRefinements

    def next
      # :nocov:
      if stateful.channel_busy
        stateful.astm_channel_connection.putc IoConstants::NACK
        stateful.actor_delegate.logger.trace('[SENT] <NACK>')
        transition_to(AstmStates::Idle)
      else
        stateful.channel_busy = true
        stateful.astm_channel_connection.putc IoConstants::ACK
        stateful.actor_delegate.logger.trace('[SENT] <ACK>')
        #At every enquire, frame number assumes value 1 according to protocol specifications
        stateful.frame_number = 1
        transition_to AstmStates::ReceiveBranch::Waiting
      end
    end

    def enter
      reset_timeout 30
      # :nocov:
      stateful.actor_delegate.logger.debug('I\'m currently on Awake State')

    end

    def exit
      # :nocov:
      stateful.actor_delegate.logger.debug('I\'m leaving from Awake State')
      # :nocov:
    end
  end
