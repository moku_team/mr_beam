require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/custom_errors'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


#Inclusions
require 'mr_beam/modules/astm_states/astm_states'

class AstmStates::ReceiveBranch::FrameReceived < BaseAstmState
  using MrBeam::ObjectRefinements

  #https://ruby-doc.org/stdlib-1.9.3/libdoc/timeout/rdoc/Timeout.html
  def next
    return if stateful.current_read_message.blank?

    # transition_to(AstmStates::Idle) if timeout_expired? || stateful.current_read_message == IoConstants::EOT
    begin
      process_result = process_message(stateful.current_read_message)
      if process_result.present?
        stateful.actor_delegate.channel.tx_buffer_mutex.synchronize do
          stateful.actor_delegate.rx(stateful.payload)
        end
        stateful.frame_number = stateful.frame_number + 1
        stateful.astm_channel_connection.putc IoConstants::ACK
        stateful.actor_delegate.logger.trace('[SENT] <ACK>')
      else
        # :nocov:
        #This is a dead branch. If flow goes here something is going wrong, so is correct to send a NACK response
        stateful.astm_channel_connection.putc IoConstants::NACK
        stateful.actor_delegate.logger.trace('[SENT] <NACK>')

        # :nocov:
      end
      transition_to(AstmStates::ReceiveBranch::Waiting)
    rescue AstmErrors::WrongCs1Error, AstmErrors::WrongCs2Error, AstmErrors::MessageDoesntBeginWithStxError, AstmErrors::InvalidFrameNumber, AstmErrors::MissingEtbOrEtxError, AstmErrors::MissingLastCRError, AstmErrors::MissingLastLFError => e

      stateful.actor_delegate.logger.warn(e.to_s)
      stateful.astm_channel_connection.putc IoConstants::NACK
      stateful.actor_delegate.logger.trace('[SENT] <NACK>')
      transition_to AstmStates::ReceiveBranch::Waiting
      return
    end
  end

  def enter
    stateful.actor_delegate.logger.debug('I\'m currently on Frame received State')
    reset_timeout(30)
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.trace('I\'m leaving from Frame received State')
    # :nocov:
  end


  private

  def reset_frame_variables
    stateful.checksum_counter = 0x0
    stateful.checksum_values = {cs1: nil, cs2: nil}
    stateful.payload=''.force_encoding('binary')
    reset_timeout 30
  end

  #message should be an astm message properly, not enq, ack, nack or eot
  def process_message _message_

    reset_frame_variables

    if _message_.present?
      # first of all I prepare an array of bytes

      bytes__=[]
      _message_.force_encoding('binary')

      _message_.each_byte do |byte_|
        bytes__ << byte_
      end

      # STX expected as first byte
      array_index = 0
      begin
        process_stx bytes__, array_index
      rescue => e
        raise e
      end

      process_frame bytes__, array_index

      #if the execution is here, everything is ok
      true
    else
      #false is only for be sure, result should be checked on Error rising
      # :nocov:
      false
      # :nocov:
    end
  end



  def process_frame bytes__, array_index
    #Preview frame
    read_message = "".force_encoding('binary')
    bytes__.each do |byte_|
      read_message << byte_
    end
    readable_message = read_message.gsub(IoConstants::ACK.chr, '<ACK>').gsub(IoConstants::NACK.chr, '<NACK>').gsub(IoConstants::ENQ.chr, '<ENQ>').gsub(IoConstants::EOT.chr, '<EOT>').gsub(IoConstants::CR.chr, '<CR>').gsub(IoConstants::LF.chr, '<LF>').gsub(IoConstants::STX.chr, '<STX>').gsub(IoConstants::ETB.chr, '<ETB>').gsub(IoConstants::ETX.chr, '<ETX>') unless read_message.blank?
    stateful.actor_delegate.logger.trace("process_frame <<< #{readable_message}")

    case stateful.actor_delegate.channel.astm_message_frame_template
    when 'highflexx'
      array_index = process_highflexx_frame bytes__, array_index  # WARNING! this index includes <CR><LF> as the last 2 characters that must be stripped stripped from the payload!
    else  # standard - default behaviour
      array_index = process_standard_frame bytes__, array_index
    end

    array_index
  end


  def process_standard_frame bytes__, array_index

    #Frame number expected as second byte
    array_index += 1
    process_frame_number bytes__, array_index


    #From now till ETB or ETX is payload
    array_index += 1
    array_index = process_payload bytes__, *array_index


    #here there is an etb or an etx
    # array_index increment is done by the process_payload function
    process_etb_or_etx bytes__, array_index


    #now must compute checksum byte
    stateful.checksum_values = compute_checksum_byte

    #now I have to compare it with 2 bytes received next
    array_index += 1
    process_cs1 bytes__, array_index

    array_index += 1
    process_cs2 bytes__, array_index

    array_index += 1
    process_last_cr bytes__, array_index

    array_index += 1
    process_last_lf bytes__, array_index

    array_index
  end

  def process_highflexx_frame bytes__, array_index

    #From here till <CR><LF> is payload
    array_index += 1
    array_index = process_highflexx_payload bytes__, *array_index

    #now must compute checksum byte
    stateful.checksum_values = compute_checksum_byte

    stateful.actor_delegate.logger.debug(" >> payload read before checksum: #{stateful.payload.encode("utf-8", invalid: :replace, undef: :replace, replace: '?')}")
    stateful.actor_delegate.logger.debug(" >> computed checksums: \n -- CS1: #{stateful.checksum_values[:cs1]} \n -- CS2: #{stateful.checksum_values[:cs2]}")


    #now I have to compare it with 2 bytes received next
    array_index += 1
    #process_cs1 bytes__, array_index
    array_index += 1
    #process_cs2 bytes__, array_index

    # here there is an etx
    array_index += 1
    process_etb_or_etx bytes__, array_index
    inject_endline_to_payload_after_computing_checksum
    array_index
  end


def inject_endline_to_payload_after_computing_checksum
  stateful.payload << "\r"
end



  # --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * ---

  #calculate first or second byte of checksum based of counter value
  def compute_checksum_byte
    #checksum is based on counter modulo 256, so is a byte
    stateful.checksum_counter %= 256

    #I need to have 2 integer that contains the value of the 2 sections of 4 bits from the counter modulo 256

    #To reach the target, I shift left or right bits of the integer and I take modulo 16.
    #Shift is necessary beacuse i always want to have the 4 bits on the last significant place

    #I don't need to put a modulo 16 operation here because checksum counter is modulo 256 and a shift of 4 position cause a division by 16. 256/16 = 16 so available range is 0 <= cs1 < 15
    cs1= stateful.checksum_counter >> 4

    #for cs2 I must use modulo 16 because rails uses 4 bytes for store an integer numbers and I must take last significant byte
    cs2= stateful.checksum_counter % 16

    #now I have to obtain hex value of cs1 and cs2
    cs1= cs1.to_s(16).upcase
    cs2= cs2.to_s(16).upcase

    {cs1: cs1, cs2: cs2}
  end

  def self.is_etb_or_etx? _byte_
    _byte_ == IoConstants::ETB || _byte_ == IoConstants::ETX
  end

  def self.is_eot? _byte_
    _byte_ == IoConstants::EOT
  end

  def self.is_cr_byte_? _byte_
    _byte_ == IoConstants::CR
  end

  def self.is_lf_byte_? _byte_
    _byte_ == IoConstants::LF
  end

  # +-----------------------------------------------------------------------------------------------------------------+
  # +-----------------------------PRIVATE_METHODS_OVER_HERE-----------------------------------------------------------+
  # +-----------------------------------------------------------------------------------------------------------------+

  private

  def process_stx bytes__, array_index
    if bytes__[array_index] == IoConstants::STX
      stateful.actor_delegate.logger.trace("[READ] <STX>")
    else
      stateful.actor_delegate.logger.debug("[READ] #{bytes__[array_index]} instead of <STX>")
      raise AstmErrors::MessageDoesntBeginWithStxError
    end
  end

  def process_frame_number bytes__, array_index
    #range check, mind that frame number is binary ASCII representation of the number so range is different
    frame_number_candidate = bytes__[array_index]
    if frame_number_candidate.between?('0'.ord,'7'.ord)
      stateful.frame_number = bytes__[array_index]
      stateful.checksum_counter = bytes__[array_index]
    else
      raise AstmErrors::InvalidFrameNumber
    end
  end

  def process_payload bytes__, array_index
    #range check, mind that frame number is binary ASCII representation of the number so range is different


    while array_index <= bytes__.length && !AstmStates::ReceiveBranch::FrameReceived.is_etb_or_etx?(bytes__[array_index]) #&& bytes__[array_index]!= IoConstants::LF
      raise AstmErrors::MissingEtbOrEtxError if bytes__[array_index] == IoConstants::LF && bytes__[array_index-1] == IoConstants::CR
      stateful.payload << bytes__[array_index]
      stateful.checksum_counter += bytes__[array_index]

      array_index += 1
    end
    array_index
  end

  def process_highflexx_payload bytes__, array_index
    #range check, mind that frame number is binary ASCII representation of the number so range is different
    #
    # I have to read till <CR><LF> without inserting into the payload but keeping the array_index updated

    if array_index > 0
      stateful.checksum_counter += bytes__[array_index-1] # STX counts for checksum
    end
    while array_index <= bytes__.length
      if (bytes__[array_index] == IoConstants::CR ) && ( bytes__[array_index+1] == IoConstants::LF)
        stateful.checksum_counter += bytes__[array_index]
        stateful.checksum_counter += bytes__[array_index+1]
        array_index += 1
        break
      else
        stateful.payload << bytes__[array_index]
        stateful.checksum_counter += bytes__[array_index]
        array_index += 1
      end

    end
    array_index
  end

  def process_etb_or_etx bytes__, array_index

    if AstmStates::ReceiveBranch::FrameReceived.is_etb_or_etx?  bytes__[array_index]
      stateful.actor_delegate.logger.trace("[READ] <ETX> or <ETB>")
      stateful.checksum_counter += bytes__[array_index]
    else
      # :nocov: redundant, here to make code stronger
      raise AstmErrors::MissingEtbOrEtxError
      # :nocov:
    end

  end

  def process_cs1 bytes__, array_index
    stateful.actor_delegate.logger.debug(" >> process_cs1 bytes__[array_index].chr: #{bytes__[array_index].chr}")

    if stateful.checksum_values[:cs1] !=  bytes__[array_index].chr
      raise AstmErrors::WrongCs1Error
    end
  end

  def process_cs2 bytes__, array_index
    stateful.actor_delegate.logger.debug(" >> process_cs2 bytes__[array_index].chr: #{bytes__[array_index].chr}")

    if stateful.checksum_values[:cs2] !=  bytes__[array_index].chr
      raise AstmErrors::WrongCs2Error
    end

  end

  def process_last_cr bytes__, array_index
    if bytes__[array_index] != IoConstants::CR
      raise AstmErrors::MissingLastCRError
    end
  end

  def process_last_lf bytes__, array_index
    if bytes__[array_index] != IoConstants::LF
      raise AstmErrors::MissingLastLFError
    end
  end
end

