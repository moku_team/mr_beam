require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


class AstmStates::SendBranch::DataToSend < BaseAstmState
  using MrBeam::ObjectRefinements
  def next
    if stateful.channel_busy || stateful.actor_delegate.channel.tx_buffer.blank?
      transition_to AstmStates::Idle
    else
      stateful.frame_number = 1
      compose_messages stateful.actor_delegate.channel.tx_buffer.first[:message]
      stateful.astm_channel_connection.putc IoConstants::ENQ
      stateful.actor_delegate.logger.trace('[SENT] <ENQ>')
      reset_timeout(15)
      transition_to AstmStates::SendBranch::Waiting
    end

  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on SendBranch::DataToSend State')
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from SendBranch::DataToSend State')
    # :nocov:
  end

  private

  def compose_messages full_communication_payload
    stateful.current_sending_message_raw_messages_length = 0
    stateful.current_array_of_frame_to_send = []

    if stateful.actor_delegate.channel.astm_message_mode == 'Unpacked' # send each row as a message
      lines = full_communication_payload.gsub("\r\n", "\n").gsub("\r", "\n").lines || []
      stateful.current_sending_message_raw_messages_length= lines.length

      index_ = 0
      lines.each{ |line| index_ = enqueue_message_on_current_frame_to_send(line, index_) }
    # lines contains the right frame - wrapped - messages
    elsif stateful.actor_delegate.channel.astm_message_mode == 'Packed'  # send all content in a single message
      stateful.current_sending_message_raw_messages_length= 1
      enqueue_message_on_current_frame_to_send full_communication_payload
    end
  end

  def enqueue_message_on_current_frame_to_send message_payload_, passed_index_=0
    message_payload_lenght_ =  message_payload_.length
    number_of_frames_to_create =  message_payload_lenght_ / 240 # length is an integer, so the resul of the division will be the integer part of the number e.g 0.999 ~> 0
    number_of_frames_to_create +=1 if message_payload_lenght_ % 240 != 0 # so I have to add last message.

    (0..(number_of_frames_to_create - 1)).each do |index_|
      offset_ = 240 * index_
      starting_position = 0 + offset_
      ending_position = 239 + offset_
      payload_ = message_payload_[starting_position..ending_position].gsub(/\n/,"\r")

      stateful.current_array_of_frame_to_send.push(compose_frame payload_, (passed_index_+ index_ )%8, (index_==number_of_frames_to_create-1))
    end
    passed_index_+number_of_frames_to_create %8
  end


  def compose_frame payload_row, index_=0, is_the_last_message_of_the_transmission = true
    case stateful.actor_delegate.channel.astm_message_frame_template
    when 'highflexx'
      compose_highflexx_frame payload_row, index_, is_the_last_message_of_the_transmission
    else # 'standard'
      compose_standard_frame payload_row, index_, is_the_last_message_of_the_transmission
    end
  end



  def compose_standard_frame payload_row, index_=0, is_the_last_message_of_the_transmission = true

    message = ''.force_encoding('binary')

    #STX
    message << IoConstants::STX

    #FRAME NUMBER
    #index starts from 0, frame number from 1
    #frame number is modulo 8

    message <<  (build_frame_number index_).force_encoding('binary')

    #MESSAGE
    payload_ = stateful.actor_delegate.channel.astm_message_mode == 'Unpacked' ? "#{payload_row.strip}\r" : payload_row.strip
    message << (build_message "#{payload_}").force_encoding('binary')

    #ETB or ETX
    if is_the_last_message_of_the_transmission
      message << (build_etx).chr.force_encoding('binary')
    else
      message << (build_etb).chr.force_encoding('binary')
    end

    #CHECKSUM
    checksum_values = build_checksum_based_on_the_message message


    message << checksum_values[:cs1].force_encoding('binary')
    message << checksum_values[:cs2].force_encoding('binary')

    #CR
    message << IoConstants::CR.chr.force_encoding('binary')

    #LF
    message << IoConstants::LF.chr.force_encoding('binary')

    #now I have to return the message that has been built
    message
  end



  def compose_highflexx_frame payload_row, index_=0, is_the_last_message_of_the_transmission = true
    message = ''.force_encoding('binary')

    #STX
    message << IoConstants::STX

    #MESSAGE
    message << (build_message "#{payload_row.strip}").force_encoding('binary')

    #CR
    message << IoConstants::CR.chr.force_encoding('binary')
    #LF
    message << IoConstants::LF.chr.force_encoding('binary')

    #CHECKSUM
    checksum_values = build_checksum_based_on_the_message message, true

    message << checksum_values[:cs1].force_encoding('binary')
    message << checksum_values[:cs2].force_encoding('binary')

    #ETX
    message << (build_etx).chr.force_encoding('binary')

    #now I have to return the message that has been built
    message
  end


  def build_frame_number row_index
    frame_number = (row_index.+1) % 8
    stateful.sending_checksum_counter = get_char_by_fixnum(frame_number).ord
    (get_char_by_fixnum frame_number)
  end

  #this method update sending_checksum_counter and return same message
  def build_message message
    message.each_byte do |byte|
      stateful.sending_checksum_counter += byte
    end
    message
  end

  #this method update sending_checksum_counter and return the right end of transmission character
  def build_etx
    stateful.sending_checksum_counter += IoConstants::ETX
    IoConstants::ETX
  end

  #this method update sending_checksum_counter and return the right end of transmission character
  def build_etb
    stateful.sending_checksum_counter += IoConstants::ETB
    IoConstants::ETB
  end

  def build_checksum_based_on_the_message message, is_highflexx=false
    use_full_message_ = is_highflexx
    counter = BaseAstmState.calculate_checksum_when_message_is_already_built message, use_full_message_
    counter %= 256

    #I don't need to put a modulo 16 operation here because checksum counter is modulo 256 and a shift of 4 position cause a division by 16. 256/16 = 16 so available range is 0 <= cs1 < 15
    cs1= counter >> 4

    #for cs2 I must use modulo 16 because rails uses 4 bytes for store an integer numbers and I must take last significant byte
    cs2= counter % 16

    #now I have to obtain hex value of cs1 and cs2
    cs1= cs1.to_s(16).upcase
    cs2= cs2.to_s(16).upcase

    {cs1: cs1, cs2: cs2}
  end

  #TODO QUESTA È UNA DUPLICAZIONE DI CODICE, RENDERE MODULARE!!
  #calculate first or second byte of checksum based of counter value
  def build_checksum_values
    #checksum is based on counter modulo 256, so is a byte
    stateful.sending_checksum_counter %= 256

    #I need to have 2 integer that contains the value of the 2 sections of 4 bits from the counter modulo 256

    #To reach the target, I shift left or right bits of the integer and I take modulo 16.
    #Shift is necessary beacuse i always want to have the 4 bits on the last significant place

    #I don't need to put a modulo 16 operation here because checksum counter is modulo 256 and a shift of 4 position cause a division by 16. 256/16 = 16 so available range is 0 <= cs1 < 15
    cs1= stateful.sending_checksum_counter >> 4

    #for cs2 I must use modulo 16 because rails uses 4 bytes for store an integer numbers and I must take last significant byte
    cs2= stateful.sending_checksum_counter % 16

    #now I have to obtain hex value of cs1 and cs2
    cs1= cs1.to_s(16).upcase
    cs2= cs2.to_s(16).upcase

    {cs1: cs1, cs2: cs2}
  end

  def get_char_by_fixnum _number
    chars= ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    chars[_number]
  end

end
