require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


class AstmStates::SendBranch::WaitingWriteResponse < BaseAstmState
  def next
    read

    if stateful.timeout_expired?
      # :nocov:
      stateful.actor_delegate.logger.info('Timeout expired on SendBranch::WaitingWriteResponse state')
      # :nocov:
      stateful.channel_busy= false
      stateful.astm_channel_connection.putc IoConstants::EOT
      # logging
      stateful.actor_delegate.logger.trace('[SENT] <EOT>')
      transition_to AstmStates::Idle




    elsif stateful.current_read_message == IoConstants::NACK.chr
      # :nocov:
      stateful.actor_delegate.logger.debug('NACK received on SendBranch::WaitingWriteResponse state')
      # :nocov:
      stateful.sending_retries_count += 1
      transition_to AstmStates::SendBranch::OldFrameSetUp

    elsif stateful.current_read_message == IoConstants::ACK.chr
      #message has been sent, can return to NextFrameSetUp
      stateful.current_array_of_frame_to_send.shift
      current_frame_to_send = nil #unused
      stateful.sending_retries_count =0
      stateful.frame_number += 1
      transition_to AstmStates::SendBranch::NextFrameSetUp
    elsif stateful.current_read_message == IoConstants::EOT.chr
      #IGNORE - probably is a duplicate, go next
      stateful.sending_retries_count = 0
      transition_to AstmStates::SendBranch::NextFrameSetUp
    end

  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on SendBranch::WaitingWriteResponse State')
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from SendBranch::WaitingWriteResponse State')
    # :nocov:
  end
end
