require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'mr_beam/transmit_outcome'
require 'byebug'


class AstmStates::SendBranch::NextFrameSetUp < BaseAstmState
  using MrBeam::ObjectRefinements
  def next
    #if exec only if all message has been processed ###DONE (blank because is all sent)
    if stateful.current_array_of_frame_to_send.blank?

      stateful.actor_delegate.channel.tx_buffer_mutex.synchronize do
        processed_message = stateful.actor_delegate.channel.tx_buffer.shift
        stateful.actor_delegate.logger.debug('Message removed from tx_buffer')
        # Call the transmit callback
        unless processed_message[:block].nil?
          processed_message[:block].call MrBeam::TransmitOutcome.new
          stateful.actor_delegate.logger.debug('Transmit callback called')
        end
        stateful.actor_delegate.channel.set_connection_status(:outbound, :connected)
        stateful.actor_delegate.logger.debug("Outbound connection status = #{stateful.actor_delegate.channel.get_connection_status[:outbound]}")
        stateful.current_frame_to_send = nil
      end

      stateful.astm_channel_connection.putc IoConstants::EOT
      stateful.actor_delegate.logger.trace('[SENT] <EOT>')

      transition_to AstmStates::Idle

    else
      stateful.current_frame_to_send = stateful.current_array_of_frame_to_send.first
      transition_to AstmStates::SendBranch::FrameReady
    end
  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on SendBranch::NextFrameSetup State')
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from SendBranch::NextFrameSetup State')
    # :nocov:
  end




end
