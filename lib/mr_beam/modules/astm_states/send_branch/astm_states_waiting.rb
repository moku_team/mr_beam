require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


class AstmStates::SendBranch::Waiting < BaseAstmState
  def next
    read

    if stateful.timeout_expired?
      stateful.channel_busy= false
      stateful.astm_channel_connection.putc IoConstants::EOT
      stateful.actor_delegate.logger.trace('[SENT] <EOT>')

      transition_to AstmStates::Idle
    elsif stateful.current_read_message == IoConstants::ENQ.chr || stateful.current_read_message == IoConstants::NACK.chr
      # :nocov:
      sleep (3 + rand(0..2))
      stateful.actor_delegate.logger.debug('ENQ or NACK received on SendBranch::Waiting state - channel challenge lost')
      # :nocov:
      transition_to AstmStates::Idle
    elsif stateful.current_read_message == IoConstants::ACK.chr
      # :nocov:
      stateful.actor_delegate.logger.debug('SendBranch::WaitingState can leave the control to NextFrameSetup state')
      # :nocov:
      stateful.sending_retries_count = 0
      transition_to AstmStates::SendBranch::NextFrameSetUp
    elsif stateful.current_read_message == IoConstants::EOT.chr
      # :nocov:
      stateful.actor_delegate.logger.debug('SendBranch::WaitingState can leave the control to NextFrameSetup state')
      # :nocov:
      stateful.sending_retries_count = 0
      transition_to AstmStates::Idle
    else # nil
      # :nocov:
      unless stateful.current_read_message.blank?
        stateful.actor_delegate.logger.warn('Unexpected condition in SendBranch::Waiting next')
        readable_message = stateful.current_read_message
                                   .gsub(IoConstants::ACK.chr,  '<ACK>')
                                   .gsub(IoConstants::NACK.chr, '<NACK>')
                                   .gsub(IoConstants::ENQ.chr,  '<ENQ>')
                                   .gsub(IoConstants::EOT.chr,  '<EOT>')
                                   .gsub(IoConstants::CR.chr,   '<CR>')
                                   .gsub(IoConstants::LF.chr,   '<LF>')
                                   .gsub(IoConstants::STX.chr,  '<STX>')
                                   .gsub(IoConstants::ETB.chr,  '<ETB>')
                                   .gsub(IoConstants::ETX.chr,  '<ETX>')
        stateful.actor_delegate.logger.debug( "[CONTEXT] stateful.current_read_message: \n #{readable_message}")
      end
      # :nocov:
    end

  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on SendBranch::Waiting State')
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from SendBranch::Waiting State')
    # :nocov:
  end
end
