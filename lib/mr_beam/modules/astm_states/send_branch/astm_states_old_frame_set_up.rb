require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


class AstmStates::SendBranch::OldFrameSetUp < BaseAstmState
  def next
    if stateful.sending_retries_count == 6
      stateful.astm_channel_connection.putc IoConstants::EOT
      stateful.actor_delegate.logger.trace('[SENT] <EOT>')

      transition_to AstmStates::Idle
    else
      transition_to AstmStates::SendBranch::FrameReady
    end
  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on SendBranch::FrameReady State')
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from SendBranch::FrameReady State')
    # :nocov:
  end
end
