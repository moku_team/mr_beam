require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'


class AstmStates::SendBranch::FrameReady < BaseAstmState
  def next

    reset_timeout 15

    # :nocov:
    readable_message = stateful.current_frame_to_send.gsub("\r", "\n").gsub(IoConstants::ACK.chr, '<ACK>').gsub(IoConstants::NACK.chr, '<NACK>').gsub(IoConstants::ENQ.chr, '<ENQ>').gsub(IoConstants::EOT.chr, '<EOT>').gsub(IoConstants::CR.chr, '<CR>').gsub(IoConstants::LF.chr, '<LF>').gsub(IoConstants::STX.chr, '<STX>').gsub(IoConstants::ETB.chr, '<ETB>').gsub(IoConstants::ETX.chr, '<ETX>') unless stateful.current_frame_to_send.blank?
    stateful.actor_delegate.logger.trace("[SENT] #{readable_message}")

    begin
      stateful.astm_channel_connection.print stateful.current_frame_to_send
    rescue => exc_
      Airbrake.notify_sync(exc_)
      raise exc_
    end


    # :nocov:

    transition_to AstmStates::SendBranch::WaitingWriteResponse

  end

  def enter
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on SendBranch::FrameReady State')
    # :nocov:
  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from SendBranch::FrameReady State')
    # :nocov:
  end
end
