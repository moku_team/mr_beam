require 'state_pattern'
require 'mr_beam/io_constants'
require 'byebug'

require 'mr_beam/modules/astm_states/receive_branch/astm_states_awake'

require 'mr_beam/modules/astm_states/astm_states'

# next()  - go to next step
# enter() - run when execution is entering in the class that represent the state
# exit()  - run when execution is going out of the class that represent the state

# main state object can be used using ''stateful'' variable that permit to invoke methods of invoker object (so use accessors :) )


module AstmStates

  # +--------------------------------------------------------------------+
  # +--+ Idle state stay the middle of the protocol, like Switzerland +--+
  # +--------------------------------------------------------------------+


  class Idle < BaseAstmState
    using MrBeam::ObjectRefinements
    def next
      stateful.entered_on_idle_timestamp ||= Time.now



      if stateful.actor_delegate.channel.stopping?
        stateful.actor_delegate.logger.debug('Soft kill requested')
        transition_to(Stopping)
      else
        if stateful.actor_delegate.channel.configuration_hash[:config][:priority_owner_branch] == 'sending'
          # stateful.actor_delegate.logger.debug('Priority: sending')
          # lis expects to lose the enquire challenge when both resources have a message to send
          if something_to_send? && is_outbound?
            transition_to(SendBranch::DataToSend)
          else
            stateful.channel_busy = false
            read_with_enquiry_handling
          end
        else # instruments -- like epicenter -- expect to have priority on writing when they have data to send
          # stateful.actor_delegate.logger.debug('Priority: receiving')

          got_enquiry_ = read_with_enquiry_handling

          if !got_enquiry_ && something_to_send? && is_outbound?
            transition_to(SendBranch::DataToSend)
          end
        end
      end
      idle_timer = stateful.actor_delegate.channel.idle_timer
      if idle_timer.present? && stateful.entered_on_idle_timestamp.present? && (Time.now - stateful.entered_on_idle_timestamp) >= idle_timer.minutes
        raise ChannelErrors::IdleTimerExpired
      end
    end

    def read_with_enquiry_handling
      read
      got_enquiry_= false
      if enquiry_detected? #|| nack_detected? -> with nack I have to stay ion enquire state
        got_enquiry_= true
        if is_inbound?
          message_evaluation_variables_reset
          transition_to(ReceiveBranch::Awake)
        else
          stateful.astm_channel_connection.putc IoConstants::NACK
          stateful.actor_delegate.logger.trace('[SENT] <NACK>')
        end
      end
      got_enquiry_
    end

    def enter
      # stateful.astm_channel_connection.print "\x027O|1|1112086401^^||^^^|||20161113000000\n\x1789\r\n"
      # :nocov:
      stateful.actor_delegate.logger.debug('I\'m currently on Idle State')
      reset_timeout(4)  # this one forces the idle state to try to read for maximum 4 seconds
      # :nocov
    end

    def exit
      # :nocov:
      stateful.entered_on_idle_timestamp = nil
      stateful.actor_delegate.logger.debug('I\'m leaving from Idle State')
      # :nocov:
    end

    private

    def nack_detected?
      result_ = stateful.current_read_message && stateful.current_read_message_contains_the_byte?(IoConstants::NACK)
      # puts 'nack_detected?: ' + (result_ ? '[YES]' : '[NO]')
      result_
    end

    def enquiry_detected?

      result_ = stateful.current_read_message && stateful.current_read_message_contains_the_byte?(IoConstants::ENQ)
      # puts 'enquiry_detected?: ' + (result_ ? '[YES]' : '[NO]')
      result_
    end

    def message_evaluation_variables_reset
      stateful.errors_here = false
      stateful.checksum_counter = 0x0
      stateful.checksum_values = {cs1: nil, cs2: nil}
      stateful.frame_number = -1
      stateful.payload=''.force_encoding('binary')
      stateful.current_read_message = ''.force_encoding('binary')
    end


    def something_to_send?
      stateful.actor_delegate.channel.tx_buffer_mutex.synchronize do
        result_= !stateful.actor_delegate.channel.tx_buffer.empty? && !stateful.channel_busy?
        # puts 'something_to_send? '+ (result_ ? '[YES]' : '[NO]')
        return result_
      end
    end

    def is_inbound?
      stateful.actor_delegate.role == :inbound || stateful.actor_delegate.role == :duplex
    end

    def is_outbound?
      stateful.actor_delegate.role == :outbound || stateful.actor_delegate.role == :duplex
    end
  end
end
