require 'mr_beam/object_refinements'
require 'io/wait'
require 'byebug'

class BaseAstmState < StatePattern::State
  using MrBeam::ObjectRefinements

  def enter
    #read_by_channel
  end


  def read
    Timeout::timeout(120) do
      concrete_read
    end
  end


  def concrete_read
    if stateful.current_state_instance.class != AstmStates::Idle && timeout_expired?
      stateful.actor_delegate.logger.info('Timeout expired on concrete read')
      transition_to AstmStates::Idle
      # return
    end

    stateful.current_read_message = nil

    read_message = "".force_encoding('binary')

    if stateful.astm_channel_connection.ready?
      begin
        while byte_read = stateful.astm_channel_connection.readbyte
          read_message << byte_read
          break if [IoConstants::NACK, IoConstants::EOT, IoConstants::ENQ, IoConstants::ACK].include? byte_read
          if stateful.actor_delegate.channel.astm_message_frame_template == 'highflexx'
            break if [IoConstants::ETX].include?( byte_read)
          else
            break if [IoConstants::LF].include?( byte_read)
          end
        end
      rescue IO::TimeoutError
        stateful.actor_delegate.logger.warn('Timeout receiving the message')
      end
    end
    readable_message = read_message.gsub(IoConstants::ACK.chr, '<ACK>').gsub(IoConstants::NACK.chr, '<NACK>').gsub(IoConstants::ENQ.chr, '<ENQ>').gsub(IoConstants::EOT.chr, '<EOT>').gsub(IoConstants::CR.chr, '<CR>').gsub(IoConstants::LF.chr, '<LF>').gsub(IoConstants::STX.chr, '<STX>').gsub(IoConstants::ETB.chr, '<ETB>').gsub(IoConstants::ETX.chr, '<ETX>') unless read_message.blank?
    stateful.actor_delegate.logger.trace("[READ] #{readable_message}") unless read_message.blank?
    stateful.current_read_message = read_message unless read_message.blank?
  end


  def reset_timeout (length_ = 30, time_=Time.now)
    stateful.execution_begins_at = time_
    stateful.timeout_in_seconds = length_
  end

  def timeout_expired?
    return false if stateful.execution_begins_at.blank?
    now = Time.now
    now > stateful.execution_begins_at + stateful.timeout_in_seconds
  end

  # +------------------------------------------------------------------------------------------------------------------+

  #this method compares each byte of current_read_message and returns true even if it finds that byte into the message
  def current_read_message_contains_the_byte? _byte_
    stateful.current_read_message.each_byte do |msg_s_byte_|
      return true if msg_s_byte_==_byte_
    end
    return false
  end


  # +------------------------------------------------------------------------------------------------------------------+

  #this method returns the string between two markers
  # string_between_markers 'beginpippoend', 'begin', 'end'
  def self.string_between_markers input_string, marker1, marker2
    input_string[/#{Regexp.escape(marker1)}(.*?)#{Regexp.escape(marker2)}/m, 1]
  end

  # +------------------------------------------------------------------------------------------------------------------+

  def self.calculate_checksum_when_message_is_already_built message_till_etx_or_etb, use_full_frame_=false
    if use_full_frame_
      frame_string = message_till_etx_or_etb
    elsif message_till_etx_or_etb.index(IoConstants::ETB.chr) != nil
      frame_string = string_between_markers message_till_etx_or_etb, IoConstants::STX.chr, IoConstants::ETB.chr
      frame_string << IoConstants::ETB.chr
    elsif message_till_etx_or_etb.index(IoConstants::ETX.chr) != nil
      frame_string = string_between_markers message_till_etx_or_etb, IoConstants::STX.chr, IoConstants::ETX.chr
      frame_string << IoConstants::ETX.chr
    end


    counter= 0
    frame_string.each_byte do |byte|
      counter += byte
    end

    counter
  end
end