require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/astm_state_machine'
require 'mr_beam/io_constants'
require 'byebug'
require 'mr_beam/custom_errors'


class AstmStates::Stopping < BaseAstmState
  def next
    # :nocov:
    stateful.actor_delegate.logger.debug('entered on Stopping next')
    raise ChannelErrors::SoftKillError
  end

  def enter

    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m currently on Stopping State')
    # :nocov:

  end

  def exit
    # :nocov:
    stateful.actor_delegate.logger.debug('I\'m leaving from Stopping State')
    # :nocov:
  end
end
