require 'mr_beam/object_refinements'
require 'state_pattern'
require 'mr_beam/io_constants'
require 'byebug'

# +-------------------------------+
# +-+ STATE DEFINITION CLASSES +--+
# +-------------------------------+

# next()  - go to next step
# enter() - run when execution is entering in the class that represent the state
# exit()  - run when execution is going out of the class that represent the state

# main state object can be used using ''stateful'' variable that permit to invoke methods of invoker object (so use accessors :) )


module AstmStates
  using MrBeam::ObjectRefinements

  # +----------------------------------------------------------------+
  # +--+ States related to Send Branch of astm's protocol diagram +--+
  # +----------------------------------------------------------------+

  module SendBranch
  end

  # --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * --- * ---

  # +-------------------------------------------------------------------+
  # +--+ States related to Receive Branch of astm's protocol diagram +--+
  # +-------------------------------------------------------------------+

   module ReceiveBranch
   end

end