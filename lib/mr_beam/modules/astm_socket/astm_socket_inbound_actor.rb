require 'mr_beam/modules/astm_socket/astm_socket_actor'
require 'mr_beam/connection_service'
require 'mr_beam/astm_state_machine'

module AstmSocket
  class AstmSocketInboundActor < AstmSocket::AstmSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :inbound
      @logger = MrBeam::ActorLogger.new channel.subject, :inbound, log_level

      super actor_name, channel, config_hash
    end
  end
end