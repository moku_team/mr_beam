require 'mr_beam/modules/astm_socket/astm_socket_actor'

module AstmSocket
  class AstmSocketOutboundActor < AstmSocket::AstmSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :outbound
      @logger = MrBeam::ActorLogger.new channel.subject, :outbound, log_level

      super actor_name, channel, config_hash
    end

    def rx
    end
  end
end