require 'mr_beam/modules/astm_socket/astm_socket_channel'
require 'mr_beam/modules/astm_socket/astm_socket_duplex_actor'

module AstmSocket
  class AstmSingleSocketChannel < AstmSocket::AstmSocketChannel
    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: AstmSocket::AstmSocketDuplexActor,
                  as: "#{@subject}_actor",
                  args: ["#{@subject}_actor", self, config, config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end