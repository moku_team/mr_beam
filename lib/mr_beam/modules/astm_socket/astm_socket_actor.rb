require 'mr_beam/channel_actor'
require 'mr_beam/connection_service'
require 'mr_beam/astm_state_machine'

module AstmSocket
  class AstmSocketActor < MrBeam::ChannelActor
    attr_accessor :role

    def initialize(actor_name, channel, config_hash)
      @current_message = ''
      @config_hash = config_hash

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')
      @socket = MrBeam::ConnectionService.create_socket(@config_hash)
      @logger.info('Socket created')

      if @role == :duplex || @role == :inbound
        @channel.set_connection_status(:inbound, :connecting)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      end

      if @role == :duplex || @role == :outbound
        @channel.set_connection_status(:outbound, :connecting)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      end

      @channel.set_actor_inactive(@actor_name, false)

      @astm_state_machine = MrBeam::AstmStateMachine.new(@socket, self)

      @logger.debug('Starting state machine loop')

      @astm_state_machine.loop_machine

      stop_actor
    end

    def rx(payload_partial)
      @logger.debug("Received payload partial: #{payload_partial}")
      message = compose_full_message(payload_partial)
      unless message.nil?
        @logger.info("Composed message:\n#{message}")
        @channel.execute_receive_callback(message)

        @logger.debug('Receive callback executed')
        @channel.set_connection_status(:inbound, :connected)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
        @logger.info('Message processed')
      end
    end

    def compose_full_message(payload_partial)

      case @channel.astm_message_frame_template
      when 'highflexx'
        message_to_return =  compose_full_highflexx_message(payload_partial)
      else  # standard - default behaviour
        message_to_return =  compose_full_standard_message(payload_partial)
      end
      message_to_return
    end

    def compose_full_standard_message(payload_partial)
      @logger.trace('compose_full_standard_message')
      @logger.trace("Payload partial:\n#{payload_partial}")
      row_identifier = payload_partial[0]
      @logger.trace("Row identifier: #{row_identifier}")
      message_to_return = nil

      if row_identifier == 'H'
        @logger.trace('It\'s a header record')
        @current_message = ''
      end

      @current_message << payload_partial

      @logger.trace("Current message after appending:\n#{@current_message}")

      last_message_chunk = ''
      @current_message.split("\r").each do |chunk|
        last_message_chunk = chunk if (/^L\|1(\|(N|R)+)?/.match(chunk)).present?
      end

      if row_identifier == 'L' || last_message_chunk[0] =='L'
        @logger.trace('It\'s a terminator record')
        message_to_return = @current_message.dup
        @current_message = ''
      end
      @logger.trace("Message to return:\n#{message_to_return}")
      message_to_return
    end

    def compose_full_highflexx_message(payload_partial)
      @logger.trace('compose_full_highflexx_message')
      @logger.trace("Payload partial:\n#{payload_partial}")
      row_identifier = payload_partial[0..2]
      @logger.trace("Row identifier: #{row_identifier}")
      message_to_return = nil

      if row_identifier == "\"H\""
        @logger.trace('It\'s a header record')
        @current_message = ''
      end

      @current_message << payload_partial

      @logger.trace("Current message after appending:\n#{@current_message}")

      last_message_chunk = ''
      @current_message.split("\r").each do |chunk|
        last_message_chunk = chunk if (/^\"L\"\|\"L\"\|\"Y\"\|\d/.match(chunk)).present?
      end

      if row_identifier == "\"L\"" || last_message_chunk[0..2] =="\"L\""
        @logger.trace('It\'s a terminator record')
        message_to_return = @current_message.dup
        @current_message = ''
      end
      @logger.trace("Highflexx message to return:\n#{message_to_return}")
      message_to_return
    end


    def finalise_actor
      @logger.warn('Actor died')

      if @role == :duplex || @role == :inbound
        @channel.set_connection_status(:inbound, :error)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      end

      if @role == :duplex || @role == :outbound
        @channel.set_connection_status(:outbound, :error)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      end
    ensure
      begin
        @socket.try :shutdown, Socket::SHUT_RDWR
      rescue Errno::ENOTCONN
        @logger.warn('The socket was already closed')
      end
      @socket.try :close
    end
  end
end