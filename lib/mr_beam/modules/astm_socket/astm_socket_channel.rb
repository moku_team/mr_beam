require 'mr_beam/socket_channel'

module AstmSocket
  class AstmSocketChannel < MrBeam::SocketChannel
    attr_accessor :astm_message_mode
    attr_accessor :astm_message_frame_template  # 'standard' || 'highflexx'


    def initialize(hash, block)
      @astm_message_mode = hash[:protocol_config][:protocol_mode]
      @astm_message_frame_template = hash[:protocol_config][:protocol_frame_template] || 'standard'

      super hash, block
    end
  end
end