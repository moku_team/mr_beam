require 'mr_beam/modules/astm_socket/astm_socket_channel'
require 'mr_beam/modules/astm_socket/astm_socket_inbound_actor'
require 'mr_beam/modules/astm_socket/astm_socket_outbound_actor'

module AstmSocket
  class AstmDoubleSocketChannel < AstmSocket::AstmSocketChannel
    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: AstmSocket::AstmSocketInboundActor,
                  as: "#{@subject}_inbound_actor",
                  args: ["#{@subject}_inbound_actor", self, config[:receiver], config[:log_level]]
              },
              {
                  type: AstmSocket::AstmSocketOutboundActor,
                  as: "#{@subject}_outbound_actor",
                  args: ["#{@subject}_outbound_actor", self, config[:sender], config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
