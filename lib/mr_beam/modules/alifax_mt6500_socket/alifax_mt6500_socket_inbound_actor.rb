require 'mr_beam/modules/alifax_mt6500_socket/alifax_mt6500_socket_actor'
require 'ruby-hl7'

module AlifaxMt6500Socket
  class AlifaxMt6500SocketInboundActor < AlifaxMt6500Socket::AlifaxMt6500SocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @config_hash = config_hash
      @logger = MrBeam::ActorLogger.new channel.subject, :inbound, log_level

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')
      @socket = MrBeam::ConnectionService.create_socket(@config_hash)
      @logger.info('Socket created')

      @channel.set_connection_status(:inbound, :connecting)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      @channel.set_actor_inactive(@actor_name, false)

      destination_worker
    end

    def finalise_actor
      @logger.warn('Actor died')

      @channel.set_connection_status(:inbound, :error)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      super
    end

    def destination_worker
      while true
        if @channel.stopping?
          break
        end

        idle_timer = @channel.idle_timer
        if idle_timer.present? && @idle_started.present? && (Time.now - @idle_started) >= idle_timer.minutes
          raise ChannelErrors::IdleTimerExpired
        end

        unless rx
          # Nothing received. Wait 1 second to avoid excessive CPU usage.
          sleep 1
        end
      end

      stop_actor
    end
  end
end
