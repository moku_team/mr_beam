require 'mr_beam/modules/alifax_mt6500_socket/alifax_mt6500_socket_channel'
require 'mr_beam/modules/alifax_mt6500_socket/alifax_mt6500_socket_duplex_actor'

module AlifaxMt6500Socket
  class AlifaxMt6500SingleSocketChannel < AlifaxMt6500Socket::AlifaxMt6500SocketChannel
    def initialize(hash, block)
      super hash, block

      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: AlifaxMt6500Socket::AlifaxMt6500SocketDuplexActor,
                  as: "#{@subject}_actor",
                  args: ["#{@subject}_actor", self, config, config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
