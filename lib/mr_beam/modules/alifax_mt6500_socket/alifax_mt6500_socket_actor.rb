require 'mr_beam/channel_actor'

module AlifaxMt6500Socket
  class AlifaxMt6500SocketActor < MrBeam::ChannelActor
    attr_accessor :idle_started

    def initialize(actor_name, channel)
      @idle_started = Time.now

      super actor_name, channel
    end

    def finalise_actor
    ensure
      begin
        @socket.try :shutdown, Socket::SHUT_RDWR
      rescue Errno::ENOTCONN
        @logger.warn('The socket was already closed')
      end
      @socket.try :close
    end


    def unwrap_message msg
      msg.gsub(/^\v/, '').gsub(/\x1C\r$/, '')
    end

    def read_message timeout_in_seconds
      transmission_begin_at = Time.now

      read_msg = ""
      current_read_byte_ = ""

      if @socket.ready?
        begin
          while (byte_read_ = @socket.readbyte) && (transmission_begin_at + timeout_in_seconds > Time.now)

            previously_read_byte_ = current_read_byte_
            current_read_byte_ = byte_read_
            read_msg << byte_read_

            if is_message_finished?(previously_read_byte_, current_read_byte_)  #TODO sembra che non stia funzionando perché crasha
              @logger.trace('Message finished')
              #@logger.trace("Payload received: \n #{read_msg}")
              break
            end
           end
        rescue EOFError
          read_msg = ""
          current_read_byte_ = ""
        end
      end

      read_msg.present? ? read_msg : nil
    end

    def current_time
      Time.now.strftime('%Y%m%d%H%M%S')
    end

    def is_message_finished?(previously_read_byte_, current_read_byte_)
      previously_read_byte_ != IoConstants::ETX && current_read_byte_ == IoConstants::ETX
    end


    def rx
      message_ = read_message 15

      if message_.blank?
        return nil
      end

      @idle_started = Time.now
      @logger.info("Message received:\n#{message_}")
      callback_result = @channel.execute_receive_callback message_
      @logger.debug('Receive callback called')
      @channel.set_connection_status(:inbound, :connected)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      @socket.print IoConstants::ACK
      @logger.info("Acknowledgment sent successfully")

      true
    end


  end
end
