require 'mr_beam/object_refinements'
require 'mr_beam/file_channel'
require 'mr_beam/modules/shared_folders/shared_folders_inbound_actor'
require 'mr_beam/modules/shared_folders/shared_folders_outbound_actor'


module SharedFolders
  class SharedFoldersFileChannel < MrBeam::FileChannel
    attr_accessor :matching_attempts, :read_attempt_interval, :external_encoding, :message_as_filename

    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @matching_attempts = config[:matching_attempts] || 4
      @read_attempt_interval = config[:read_attempt_interval] || 0.5
      @external_encoding = config[:external_encoding] || nil
      @message_as_filename = config[:message_as_filename] || false

      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: SharedFolders::SharedFoldersInboundActor,
                  as: "#{@subject}_inbound_actor",
                  args: ["#{@subject}_inbound_actor", self, config[:log_level]]
              },
              {
                  type: SharedFolders::SharedFoldersOutboundActor,
                  as: "#{@subject}_outbound_actor",
                  args: ["#{@subject}_outbound_actor", self, config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
