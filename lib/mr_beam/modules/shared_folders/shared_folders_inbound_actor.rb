require 'mr_beam/channel_actor'
require 'mr_beam/actor_logger'

module SharedFolders
  class SharedFoldersInboundActor < MrBeam::ChannelActor
    def initialize(actor_name, channel, log_level)
      @logger = MrBeam::ActorLogger.new channel.subject, :inbound, log_level
      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')

      # Check if input_folder is defined in the config. Otherwise go to sleep until spring. Maybe summer.
      # Notice: this can be useful if a shared_folder channel is used only for outbound.
      loop { sleep 1000 } if @channel.input_folder.blank?

      # Check the folder presence
      until File.directory?(@channel.input_folder)
        @logger.debug("Checking folder presence: #{@channel.input_folder}")
        sleep 5
      end

      @logger.info('Folder found')

      @channel.set_connection_status(:inbound, :connecting)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      @channel.set_actor_inactive(@actor_name, false)

      loop do
        break if @channel.stopping?

        process_folder

        sleep @channel.poll_interval
      end

      stop_actor
    end

    def finalise_actor
      @logger.warn('Actor died')
      @channel.set_connection_status(:inbound, :error)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
    end

    def process_folder
      unless File.directory?(@channel.input_folder)
        @logger.warn("Directory #{@channel.input_folder} not found")
        raise 'Folder not found'
      end

      file_name = "#{@channel.input_folder}/#{@channel.input_file_name}"

      Dir[file_name].each do |file|
        @channel.set_connection_status(:inbound, :connected)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

        next unless File.file? file

        @logger.info("File found: #{file}")

        # All these multiple reading shenanigans are necessary to ensure the stability of the file we're reading.
        # Some softwares will write the file in multiple steps, and not in one single atomic write instruction, which is
        # why we need to make sure that what we read is the final, complete content
        read_attempts = 0
        matching_attempts = 0
        previous_content = ''

        @logger.trace("Reading stability checks for file #{file}")
        # I perform up to 10 read attempts
        while read_attempts < 10
          begin
            @logger.trace("Read attempt n. #{read_attempts + 1}")
            file_content = File.read(file)
            # If the channel works with a different encoding, convert the file content to UTF-8
            unless @channel.external_encoding.blank?
              file_content = file_content
                             .force_encoding(@channel.external_encoding)
                             .encode('utf-8', invalid: :replace, undef: :replace, replace: '?')
            end
            @logger.trace("File content:\n#{file_content}")

            # If the file content matches with the content it had in the previous read attempt, I increment
            # the counter of attempts that had a matching content
            if file_content == previous_content
              @logger.trace('File contents match')
              matching_attempts += 1
              # Otherwise, I erase the matching attempts counter and update the previous content
            else
              @logger.trace('File contents do not match')
              previous_content = file_content
              matching_attempts = 0
            end

            @logger.trace("File contents have matched up #{matching_attempts} times in a row")
            # I consider the file to be stable after a customizable amount of successful matching attempts.
            if matching_attempts >= @channel.matching_attempts
              @logger.trace('The file is stable')
              @logger.info("File content: \n#{file_content}")
              # Try to execute receive callback and check if it raises an error. If it does, try to move it to failed
              # folder, otherwise processing will continue to fail processing same file
              begin
                @channel.execute_receive_callback file_content
              rescue StandardError => e
                @logger.warn("Error during execution of receive callback: #{e.inspect}")
                if (@channel.failed_folder != nil) && File.directory?( @channel.failed_folder)
                  FileUtils.mv(file, "#{@channel.failed_folder}/#{Time.now.strftime('%Y%m%d%H%M%S%L')}-#{File.basename(file)}")
                  @logger.warn("File moved to failed folder")
                else
                  @logger.warn("File don't moved to failed folder since it's missing")
                end
                raise e
              end

              @logger.debug('Receive callback executed')
              if (@channel.input_persistence_folder != nil) && File.directory?( @channel.input_persistence_folder.to_s)
                FileUtils.mv(file, "#{@channel.input_persistence_folder}/#{Time.now.strftime('%Y%m%d%H%M%S%L')}-#{File.basename(file)}")
                @logger.debug('File moved to persistence folder')
              else
                FileUtils.rm_f file
                @logger.debug('File removed')
              end
              @logger.info('File processed')
              break
            end
          rescue Errno::EBUSY, Errno::ENOENT => e
            @logger.warn("Error during reading: #{e.inspect}")
            sleep 5 # Extra sleep additional to `read_attempt_interval`.
          end

          read_attempts += 1
          sleep @channel.read_attempt_interval
        end
      end
    end
  end
end
