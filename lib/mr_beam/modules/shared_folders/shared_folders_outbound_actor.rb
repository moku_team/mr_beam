require 'mr_beam/channel_actor'
require 'mr_beam/transmit_outcome'

module SharedFolders
  class SharedFoldersOutboundActor < MrBeam::ChannelActor
    def initialize(actor_name, channel, log_level)
      @logger = MrBeam::ActorLogger.new channel.subject, :outbound, log_level
      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')

      # Check if output_folder is defined in the config. Otherwise go to sleep until spring. Maybe summer.
      # Notice: this can be useful if a shared_folder channel is used only for inbound.
      loop { sleep 1000 } if @channel.output_folder.blank?

      until File.directory?(@channel.output_folder)
        @logger.debug("Checking folder presence: #{@channel.output_folder}")
        sleep 5
      end

      @logger.info('Folder found')

      @channel.set_connection_status(:outbound, :connecting)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")

      @channel.set_actor_inactive(@actor_name, false)

      loop do
        break if @channel.stopping?

        process_queue

        sleep 1
      end

      stop_actor
    end

    def finalise_actor
      @logger.warn('Actor died')
      @channel.set_connection_status(:outbound, :error)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
    end

    def process_queue
      unless File.directory?(@channel.output_folder)
        @logger.warn("Directory #{@channel.output_folder} not found")
        raise 'Folder not found'
      end

      if @channel.message_queue.empty?
        @logger.trace('Message queue is empty')
        return
      end

      # TODO: build the correct destination (how do we behave with empty messages that must be used as filenames? 🤔)
      message_text = @channel.message_queue.first[:message]
      destination = "#{@channel.output_folder}/#{output_filename(@channel.output_file_name, message_text)}"

      if @channel.should_overwrite_dest || Dir[destination].empty?
        next_message = @channel.message_queue.first
        @logger.debug("Sending message: #{next_message[:message]}")

        send_message next_message
        @logger.info("Message sent: #{next_message[:message]}")
      else
        @logger.debug('Loop skipped as there is already a file in the output folder')
      end
    end

    def send_message(msg_hash)
      # TODO: build the correct destination
      message_text = msg_hash[:message]
      destination = "#{@channel.output_folder}/#{output_filename(@channel.output_file_name, message_text)}"
      @logger.debug("Destination: #{destination}")
      @logger.debug("Message text:\n#{message_text}")
      transmit_callback = msg_hash[:block]
      outcome = MrBeam::TransmitOutcome.new

      begin
        FileUtils.rm destination if File.exist? destination

        # The default file open mode doesn't require any explicit encoding
        file_open_mode = 'w'
        # If the channel works with a different encoding, convert the message to that encoding
        unless @channel.external_encoding.blank?
          message_text = message_text.encode(@channel.external_encoding, invalid: :replace, undef: :replace, replace: '?')
          # Add the explicit encoding to the file open mode
          file_open_mode = "w:#{@channel.external_encoding}"
        end

        retries = 0
        begin
          # If the directory existed at first, but then was deleted by an external actor during the processing, mr_beam will recreate it.
          FileUtils.mkdir_p @channel.output_folder unless Dir.exist? @channel.output_folder
          written_bytes = File.write(destination, message_text, mode: file_open_mode)
        rescue Errno::EEXIST, Errno::ENOENT => e
          retries += 1
          if retries <= 5
            @logger.trace("Error writing to file: #{e.inspect}")
            sleep 1
            retry
          else
            @logger.trace("Max retries reached. Raising exception... #{e.inspect}")
            raise e
          end
        end

        @logger.trace("#{written_bytes} bytes written")
        @channel.message_queue.shift
        @logger.trace('Message removed from the queue')
        @channel.set_connection_status(:outbound, :connected)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      rescue StandardError => e
        @logger.warn("Exception fired: #{e.inspect}")
        if transmit_callback.present?
          outcome.errors << e.inspect
          transmit_callback.call(outcome)
          @logger.debug('Transmit callback called with unsuccessful outcome')
        end

        @logger.trace('Reraising exception')
        raise e
      end

      unless transmit_callback.nil?
        if written_bytes != message_text.bytesize
          @logger.warn('The written bytes don\'t match the message byte size')
          outcome.errors << 'The written bytes don\'t match the message byte size'
        end

        transmit_callback.call(outcome)
        @logger.debug('Transmit callback called')
      end
    end

    private

    def replace_wildcards(file_name)
      file_name.gsub('[TIMESTAMP]', Time.now.strftime('%Y%m%d%H%M%S%L'))
    end

    def output_filename(file_name, message_text)
      return message_text if @channel.message_as_filename && message_text.present?

      replace_wildcards(file_name)
    end
  end
end
