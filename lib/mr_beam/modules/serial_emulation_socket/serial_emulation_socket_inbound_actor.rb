require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_actor'
require 'mr_beam/connection_service'

module SerialEmulationSocket
  class SerialEmulationSocketInboundActor < SerialEmulationSocket::SerialEmulationSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :inbound
      @logger = MrBeam::ActorLogger.new channel.subject, :inbound, log_level

      super actor_name, channel, config_hash
    end
  end
end