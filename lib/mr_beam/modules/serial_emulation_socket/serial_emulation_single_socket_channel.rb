require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_channel'
require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_duplex_actor'

module SerialEmulationSocket
  class SerialEmulationSingleSocketChannel < SerialEmulationSocket::SerialEmulationSocketChannel
    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: SerialEmulationSocket::SerialEmulationSocketDuplexActor,
                  as: "#{@subject}_actor",
                  args: ["#{@subject}_actor", self, config, config[:log_level]]
              }
          ]
      )
      @supervision_config.deploy
    end
  end
end