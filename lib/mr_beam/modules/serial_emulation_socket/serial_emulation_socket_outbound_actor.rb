require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_actor'

module SerialEmulationSocket
  class SerialEmulationSocketOutboundActor < SerialEmulationSocket::SerialEmulationSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @role = :outbound
      @logger = MrBeam::ActorLogger.new channel.subject, :outbound, log_level

      super actor_name, channel, config_hash
    end

    def rx
    end
  end
end