require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_channel'
require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_inbound_actor'
require 'mr_beam/modules/serial_emulation_socket/serial_emulation_socket_outbound_actor'

module SerialEmulationSocket
  class SerialEmulationDoubleSocketChannel < SerialEmulationSocket::SerialEmulationSocketChannel
    def initialize(hash, block)
      super hash, block
      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: SerialEmulationSocket::SerialEmulationSocketInboundActor,
                  as: "#{@subject}_inbound_actor",
                  args: ["#{@subject}_inbound_actor", self, config[:receiver], config[:log_level]]
              },
              {
                  type: SerialEmulationSocket::SerialEmulationSocketOutboundActor,
                  as: "#{@subject}_outbound_actor",
                  args: ["#{@subject}_outbound_actor", self, config[:sender], config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
