require 'mr_beam/socket_channel'

module SerialEmulationSocket
  class SerialEmulationSocketChannel < MrBeam::SocketChannel

    def initialize(hash, block)
      super hash, block
    end
  end
end