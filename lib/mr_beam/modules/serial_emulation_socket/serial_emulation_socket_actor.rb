require 'mr_beam/channel_actor'
require 'mr_beam/connection_service'

module SerialEmulationSocket
  class SerialEmulationSocketActor < MrBeam::ChannelActor
    attr_accessor :role

    def initialize(actor_name, channel, config_hash)
      @current_message = ''
      @config_hash = config_hash
      @stx = @config_hash[:stx].to_i || IoConstants::STX
      @etx = @config_hash[:etx].to_i || IoConstants::ETX
      @use_length = @config_hash[:use_length] || false

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')
      @socket = MrBeam::ConnectionService.create_socket(@config_hash)
      @logger.info('Socket created')

      if @role == :duplex || @role == :inbound
        @channel.set_connection_status(:inbound, :connecting)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      end

      if @role == :duplex || @role == :outbound
        @channel.set_connection_status(:outbound, :connecting)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      end

      @channel.set_actor_inactive(@actor_name, false)

      duplex_worker

      stop_actor
    end


    def duplex_worker
      while true
        if @channel.stopping?
          break
        end

        idle_timer = @channel.idle_timer
        if idle_timer.present? && @idle_started.present? && (Time.now - @idle_started) >= idle_timer.minutes
          raise ChannelErrors::IdleTimerExpired
        end

        if @config_hash[:priority_owner_branch] != 'sending'
          # Receiving priority (default). If both endpoint are sending, mr_beam will fail (non-ack response) and in the next loop mr_beam will grant priority to instrument (receiving).
          unless rx
            #DISABLE TX, LISTEN ONLY
            #tx
            sleep 1
          end
        else
          #DISABLE TX
          # # Sending priority. Notice: untested mode and uncommon (instrument typically have sending priority).
          # while tx
          #   sleep 1
          # end
          # rx
        end
      end
    end

    def read_message timeout_in_seconds
      transmission_begin_at = Time.now

      read_msg = "".force_encoding("ASCII-8BIT")
      current_read_byte_ = ""
      msg_started = false

      if @socket.ready?
        count = 0
        length = 1000000
        begin
          while (byte_read_ = @socket.readbyte) && (transmission_begin_at + timeout_in_seconds > Time.now)
            #previously_read_byte_ = current_read_byte_ back when i thought i needed two characters to end the transmission
            current_read_byte_ = byte_read_
            @logger.trace("%02X" % byte_read_)
            if msg_started || byte_read_ == @stx
              @logger.trace('Message started') unless msg_started

              if count == 1
                #read the second byte
                length = ("%02X" % byte_read_).to_i(16)
                @logger.trace("computed length: #{length}") if @use_length
              end

              msg_started = true
              read_msg << byte_read_
              count += 1
              if current_read_byte_ == @etx && !@use_length
                @logger.trace('Message finished')
                break
              end
              if @use_length && count >= (length + 2)
                @logger.trace('Message finished')
                break
              end
            end
          end
        rescue IO::TimeoutError
          @logger.warn('Timeout receiving the message')
        end
      end

      read_msg.present? ? read_msg : nil
    end

    def current_time
      Time.now.strftime('%Y%m%d%H%M%S')
    end

    #unused
    def is_message_finished?(previously_read_byte_, current_read_byte_)
      current_read_byte_ == IoConstants::ETX ||  current_read_byte_ == IoConstants::CETX
    end

    def rx
      message_ = read_message 60

      if message_.blank?
        return nil
      end

      @idle_started = Time.now
      @logger.info("Message received: #{message_}")
      @logger.trace("Message bytes: #{message_.bytes}")
      @channel.execute_receive_callback message_
      @logger.debug('Receive callback called')
      @channel.set_connection_status(:inbound, :connected)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      true
    end

    def tx
      #TODO: TX IS ACTUALLY NON WORKING AND DISABLED.
      sending_success = send_first_message
      return false unless sending_success

      @idle_started = Time.now
      m_ = ''
      started_at_ = Time.now

      @logger.trace('Waiting for ACK')
      while m_.blank?
        if Time.now > started_at_ + 45.seconds
          #timeout expired
          @logger.trace('ACK timeout expired')
          break
        end

        m_ = read_message 100
      end

      if m_.blank?
        @logger.warn('Empty message received instead of ACK')
      else
        hl7_payload = unwrap_message m_

        if hl7_payload.present? && is_ack?(@channel.tx_buffer.first[:message], hl7_payload) && !(hl7_payload.try(:[], 2) && hl7_payload[2].class == HL7::Message::Segment::ERR)
          @source_attempts = 0
          processed_message = @channel.tx_buffer.shift #removed from queue only if an ack is read.
          @logger.trace("ACK received: #{hl7_payload}")
          @logger.debug('Message removed from tx_buffer')

          # Done! Call the transmit callback
          unless processed_message[:block].nil?
            processed_message[:block].call MrBeam::TransmitOutcome.new
            @logger.debug('Transmit callback called')
          end
        else
          @logger.debug("Non-ACK response received: #{hl7_payload}")
          @source_attempts += 1
          if @source_attempts >= 4
            broken_message_outcome_ = MrBeam::TransmitOutcome.new
            broken_message_outcome_.errors.push ('Message error')
            @logger.warn('Message error')
            processed_message = @channel.tx_buffer.shift
            processed_message[:block].call broken_message_outcome_
            @logger.debug('Transmit callback called with unsuccessful outcome')

            # Reset attempts counter.
            @source_attempts = 0
          end
        end
      end
      # Always return true. Following `tx` call will return false if tx_buffer is empty.
      true
    end


    def finalise_actor
      @logger.warn('Actor died')

      if @role == :duplex || @role == :inbound
        @channel.set_connection_status(:inbound, :error)
        @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")
      end

      if @role == :duplex || @role == :outbound
        @channel.set_connection_status(:outbound, :error)
        @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      end
    ensure
      begin
        @socket.try :shutdown, Socket::SHUT_RDWR
      rescue Errno::ENOTCONN
        @logger.warn('The socket was already closed')
      end
      @socket.try :close
    end
  end
end