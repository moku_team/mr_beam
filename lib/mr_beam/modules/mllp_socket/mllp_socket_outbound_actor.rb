require 'mr_beam/modules/mllp_socket/mllp_socket_actor'

module MllpSocket
  class MllpSocketOutboundActor < MllpSocket::MllpSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @config_hash = config_hash
      @source_attempts = 0
      @logger = MrBeam::ActorLogger.new channel.subject, :outbound, log_level

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')
      @socket = MrBeam::ConnectionService.create_socket(@config_hash)
      @logger.info('Socket created')

      @channel.set_connection_status(:outbound, :connecting)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")

      @channel.set_actor_inactive(@actor_name, false)

      source_worker
    end

    def finalise_actor
      @logger.warn('Actor died')

      @channel.set_connection_status(:outbound, :error)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")

      super
    end

    def source_worker
      loop do
        if @channel.stopping?
          break
        end

        idle_timer = @channel.idle_timer
        if idle_timer.present? && @idle_started.present? && (Time.now - @idle_started) >= idle_timer.minutes
          raise ChannelErrors::IdleTimerExpired
        end

        # This allows the source worker to intercept EOFs even if it has nothing to send
        if @socket.ready?
          begin
            @socket.readbyte
          rescue IO::TimeoutError
            nil
          end
        end

        tx

        sleep 1
      end

      stop_actor
    end

  end
end
