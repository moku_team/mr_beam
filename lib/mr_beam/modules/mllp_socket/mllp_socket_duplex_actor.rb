require 'mr_beam/modules/mllp_socket/mllp_socket_actor'
require 'ruby-hl7'

module MllpSocket
  class MllpSocketDuplexActor < MllpSocket::MllpSocketActor
    def initialize(actor_name, channel, config_hash, log_level)
      @config_hash = config_hash
      @logger = MrBeam::ActorLogger.new channel.subject, :duplex, log_level
      @source_attempts = 0

      super actor_name, channel
    end

    def work
      @logger.info('Actor starting')
      @socket = MrBeam::ConnectionService.create_socket(@config_hash)
      @logger.info('Socket created')

      @channel.set_connection_status(:inbound, :connecting)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      @channel.set_connection_status(:outbound, :connecting)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")

      @channel.set_actor_inactive(@actor_name, false)

      duplex_worker
    end

    def finalise_actor
      @logger.warn('Actor died')

      @channel.set_connection_status(:inbound, :error)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      @channel.set_connection_status(:outbound, :error)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")

      super
    end

    def duplex_worker
      while true
        if @channel.stopping?
          break
        end

        idle_timer = @channel.idle_timer
        if idle_timer.present? && @idle_started.present? && (Time.now - @idle_started) >= idle_timer.minutes
          raise ChannelErrors::IdleTimerExpired
        end

        if @config_hash[:priority_owner_branch] != 'sending'
          # Receiving priority (default). If both endpoint are sending, mr_beam will fail (non-ack response) and in the next loop mr_beam will grant priority to instrument (receiving).
          unless rx
            tx
            sleep 1
          end
        else
          # Sending priority. Notice: untested mode and uncommon (instrument typically have sending priority).
          while tx
            sleep 1
          end
          rx
        end
      end

      stop_actor
    end

  end
end
