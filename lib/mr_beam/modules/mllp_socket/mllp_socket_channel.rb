require 'mr_beam/socket_channel'

module MllpSocket
  class MllpSocketChannel < MrBeam::SocketChannel
    attr_accessor :app_name

    def initialize(hash, block)
      super hash, block

      @app_name = hash[:app_name]
    end
  end
end
