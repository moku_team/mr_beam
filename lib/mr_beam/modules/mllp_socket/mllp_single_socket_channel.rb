require 'mr_beam/modules/mllp_socket/mllp_socket_channel'
require 'mr_beam/modules/mllp_socket/mllp_socket_duplex_actor'

module MllpSocket
  class MllpSingleSocketChannel < MllpSocket::MllpSocketChannel
    def initialize(hash, block)
      super hash, block

      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: MllpSocket::MllpSocketDuplexActor,
                  as: "#{@subject}_actor",
                  args: ["#{@subject}_actor", self, config, config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
