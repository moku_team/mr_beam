require 'mr_beam/modules/mllp_socket/mllp_socket_channel'
require 'mr_beam/modules/mllp_socket/mllp_socket_inbound_actor'
require 'mr_beam/modules/mllp_socket/mllp_socket_outbound_actor'

module MllpSocket
  class MllpDoubleSocketChannel < MllpSocket::MllpSocketChannel
    def initialize(hash, block)
      super hash, block

      config = hash[:config]
      @supervision_config = Celluloid::Supervision::Configuration.define(
          [
              {
                  type: MllpSocket::MllpSocketInboundActor,
                  as: "#{@subject}_inbound_actor",
                  args: ["#{@subject}_inbound_actor", self, config[:receiver], config[:log_level]]
              },
              {
                  type: MllpSocket::MllpSocketOutboundActor,
                  as: "#{@subject}_outbound_actor",
                  args: ["#{@subject}_outbound_actor", self, config[:sender], config[:log_level]]
              }
          ]
      )

      @supervision_config.deploy
    end
  end
end
