require 'mr_beam/channel_actor'

module MllpSocket
  class MllpSocketActor < MrBeam::ChannelActor
    attr_accessor :idle_started

    def initialize(actor_name, channel)
      @idle_started = Time.now

      super actor_name, channel
    end

    def finalise_actor
    ensure
      begin
        @socket.try :shutdown, Socket::SHUT_RDWR
      rescue Errno::ENOTCONN
        @logger.warn('The socket was already closed')
      end
      @socket.try :close
    end

    def wrap_message message_
      # ruby-hl7 has his own formatter.
      return message_.to_mllp if message_.class == HL7::Message
      # Fallback when not using ruby-hl7
      "#{IoConstants::VT.chr}#{message_}#{IoConstants::FS.chr}#{IoConstants::CR.chr}"
    end

    def unwrap_message msg
      msg.gsub(/^\v/, '').gsub(/\x1C\r$/, '')
    end

    def read_message timeout_in_seconds
      transmission_begin_at = Time.now

      read_msg = ""
      current_read_byte_ = ""

      if @socket.ready?
        begin
          while (byte_read_ = @socket.readbyte) && (transmission_begin_at + timeout_in_seconds > Time.now)
            previously_read_byte_ = current_read_byte_
            current_read_byte_ = byte_read_
            read_msg << byte_read_

            if is_message_finished?(previously_read_byte_, current_read_byte_)
              @logger.trace('Message finished')
              break
            end
          end
        rescue IO::TimeoutError
          @logger.warn('Timeout receiving the message')
        end
      end

      read_msg.present? ? read_msg : nil
    end

    def current_time
      Time.now.strftime('%Y%m%d%H%M%S')
    end

    def is_message_finished?(previously_read_byte_, current_read_byte_)
      previously_read_byte_ == IoConstants::FS && current_read_byte_ == IoConstants::CR
    end

    def is_ack? raw_message_sent, raw_reply
      return false if raw_reply.blank?

      HL7.ParserConfig[:empty_segment_is_error] = false
      reply = HL7::Message.new(raw_reply.gsub("\r\n", "\r"))

      sent_message = HL7::Message.new(raw_message_sent)
      # Check message type. At the moment we support standard ACK messages and ORL^O34 messages (i.e. replies to ORL^O33 host queries).
      message_type_correct = reply[:MSH].message_type.split('^')[0] == 'ACK' || (reply[:MSH].message_type.split('^')[0] == 'ORL' && reply[:MSH].message_type.split('^')[1] == 'O34')
      control_id_correct = reply.try(:[], :MSA) && sent_message.try(:[], 0) && reply[:MSA].control_id == sent_message[0].message_control_id
      if message_type_correct && control_id_correct
        return true
      end

      false
    end

    # Default legacy ACK standard message.
    # NOTICE: maybe it is strange to build the ACK message inside the gem, where mr_bean is usually _just_ a message routing system.
    # The ACK in HL7 is an actual message, not a flow message. Maybe we should consider to drop ack message building inside mr_beam.
    def build_ack_message message_control_id, mode_="RECEIVER", app_name="MRBEAM", protocol_version_='2.5'
      # look at https://datica.com/blog/hl7-ack-nack/
      return false unless ["SENDER", "RECEIVER"].include? mode_
      timestamp_ = current_time
      #ack_="MSH|^~\\&|#{mode_}|#{app_name}|||||ACK|#{timestamp_}|P|#{protocol_version_}\rMSA|AA|#{message_control_id}\r" #TODO decommentare per il rilascio, l'ospedale usa HLT v2.5, mentre il simulatore 2.3.1
      "MSH|^~\\&|#{mode_}|#{app_name}|||||ACK|#{timestamp_}|P|#{protocol_version_}\rMSA|AA|#{message_control_id}\r"
    end

    def send_first_message
      return false if @channel.tx_buffer.blank?

      wrapped_message = wrap_message(@channel.tx_buffer.first[:message])
      @socket.write wrapped_message
      @logger.info("Message sent:\n#{wrapped_message}")
      @channel.set_connection_status(:outbound, :connected)
      @logger.debug("Outbound connection status = #{@channel.get_connection_status[:outbound]}")
      true
    end

    def rx
      message_ = read_message 15 #TODO: era 30 ma probabilmente cobas va in timeout in attesa di oml dopo

      if message_.blank?
        return nil
      end

      @idle_started = Time.now
      @logger.info("Message received:\n#{message_}")
      callback_result = @channel.execute_receive_callback message_
      @logger.debug('Receive callback called')
      @channel.set_connection_status(:inbound, :connected)
      @logger.debug("Inbound connection status = #{@channel.get_connection_status[:inbound]}")

      HL7.ParserConfig[:empty_segment_is_error] = false
      hl7_message = HL7::Message.new(message_)
      message_control_id = hl7_message[:MSH].message_control_id

      if callback_result.is_a?(Hash) && callback_result.has_key?(:response)
        # Send custom response to the sender.
        # This is usually useful to send an ACK or a NAK depending on the processing done in the callback.

        response = callback_result[:response]
        if response.class == HL7::Message
          # Set message_control_id in the acknowledgment to match with the received message, accordingly to HL7 standard.
          response[:MSA].control_id = message_control_id
        end
      else
        # Default legacy ACK standard message.
        # NOTICE: maybe it is strange to build the ACK message inside the gem, where mr_bean is usually _just_ a message routing system.
        # The ACK in HL7 is an actual message, not a flow message. Maybe we should consider to drop ack message building inside mr_beam.
        response = build_ack_message message_control_id, mode_='RECEIVER', app_name=(@channel.app_name || 'MR_BEAM')
      end

      if response.present?
        wrapped_message = wrap_message(response)
        @socket.print wrapped_message
        @logger.info("Acknowledgment response sent: #{wrapped_message}")
      else
        # Don't send any response. This is useful in some remote use cases (e.g. with Cobas).
        @logger.info("No acknowledgment response sent.")
      end
      true
    end

    def tx
      sending_success = send_first_message
      return false unless sending_success

      @idle_started = Time.now
      m_ = ''
      started_at_ = Time.now

      @logger.trace('Waiting for ACK')
      while m_.blank?
        if Time.now > started_at_ + 45.seconds
          #timeout expired
          @logger.trace('ACK timeout expired')
          break
        end

        m_ = read_message 100
      end

      if m_.blank?
        @logger.warn('Empty message received instead of ACK')
      else
        hl7_payload = unwrap_message m_

        if hl7_payload.present? && is_ack?(@channel.tx_buffer.first[:message], hl7_payload) && !(hl7_payload.try(:[], 2) && hl7_payload[2].class == HL7::Message::Segment::ERR)
          @source_attempts = 0
          processed_message = @channel.tx_buffer.shift #removed from queue only if an ack is read.
          @logger.trace("ACK received: #{hl7_payload}")
          @logger.debug('Message removed from tx_buffer')

          # Done! Call the transmit callback
          unless processed_message[:block].nil?
            processed_message[:block].call MrBeam::TransmitOutcome.new
            @logger.debug('Transmit callback called')
          end
        else
          @logger.debug("Non-ACK response received: #{hl7_payload}")
          @source_attempts += 1
          if @source_attempts >= 4
            broken_message_outcome_ = MrBeam::TransmitOutcome.new
            broken_message_outcome_.errors.push ('Message error')
            @logger.warn('Message error')
            processed_message = @channel.tx_buffer.shift
            processed_message[:block].call broken_message_outcome_
            @logger.debug('Transmit callback called with unsuccessful outcome')

            # Reset attempts counter.
            @source_attempts = 0
          end
        end
      end
      # Always return true. Following `tx` call will return false if tx_buffer is empty.
      true
    end

  end
end
