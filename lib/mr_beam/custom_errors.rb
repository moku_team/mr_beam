# +---------------------+
# +-------ERRORS--------+
# +---------------------+

# Define here custom error or exceptions
module AstmErrors
  class InvalidFrameNumber < IOError
    def to_s
      p 'unable to complete operation because the frame number doesn\'t be in 0-7 interval'
    end
  end

  class MessageDoesntBeginWithStxError < IOError
    def to_s
      p 'unable to complete operation because the message doesn\'t begin with the STX character'
    end
  end

  class MissingEtbOrEtxError < IOError
    def to_s
      p 'unable to complete operation because ETB - End of Transmission Block - or ETX - End of Transmission - is missing on the message'
    end
  end

  class MissingLastCRError < IOError
    def to_s
      p 'unable to complete operation because last CR is missing on the message'
    end
  end

  class MissingLastLFError < IOError
    def to_s
      p 'unable to complete operation because last LF is missing on the message'
    end
  end

  class WrongCs1Error < IOError
    def to_s
      p 'unable to complete operation cause of wrong cs1'
    end
  end

  class WrongCs2Error < IOError
    def to_s
      p 'unable to complete operation cause of wrong cs2'
    end
  end
end

module ConnectionErrors
  class CannotEnstablishCommunication < IOError
    def to_s
      p 'Cannot enstablish communication due to network failure or connection misconfiguration'
    end
  end
end

module ChannelErrors
  class ChannelConfigurationError < StandardError
    def to_s
      p 'Channel configuration is wrong or missing'
    end
  end

  class ChannelStopTimeOut < StandardError
    attr_reader :non_stopping_channels

    def initialize(message, non_stopping_channels = [])
      super(message)
      @non_stopping_channels = non_stopping_channels
    end

    def to_s
      p "Stop unsuccessful for the channel(s) #{@non_stopping_channels.map(&:subject).join(', ')}"
    end
  end

  class SoftKillError < StandardError
    def to_s
      p 'Kill request received'
    end
  end

  class IdleTimerExpired < StandardError
    def to_s
      p 'Channel remained idle for too long'
    end
  end
end
