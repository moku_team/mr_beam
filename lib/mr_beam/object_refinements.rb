module MrBeam::ObjectRefinements

  refine Object do

    def present?; (self !=nil && self !='' && self != {} && self != [] && self != false ); end

    def blank?; !present? ; end
  end


end