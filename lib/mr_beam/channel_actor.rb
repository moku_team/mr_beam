require 'celluloid'

class MrBeam::ChannelActor
  include Celluloid
  finalizer :finalise_actor
  attr_reader :channel
  attr_reader :logger

  def initialize(actor_name, channel)
    @actor_name = actor_name
    @channel = channel

    @channel.set_actor_inactive(@actor_name, true)

    async.start
  end

  def start
    begin
      work
    rescue => exc
      # Report the exception to the actor logger
      @logger.warn exc

      # Reraise the exception to Celluloid
      raise exc
    end
  end

  def work
  end

  def stop_actor
    @logger.warn('Channel stopped')
    @channel.signal_actor_stopped @actor_name
  end

  def finalise_actor
  end
end