module IoConstants
  READ_TIMEOUT = 15

  # CR    = "\x0D" # 13 b10
  # LF    = "\x0A" # 10 b10
  # ENQ   = "\x05" # 5  b10
  # ACK   = "\x06" # 6  b10
  # NACK  = "\x15" # 21 b10
  # STX   = "\x02" # 2  b10
  # ETX   = "\x03" # 3  b10
  # ETB   = "\x17" # 23 b10
  # EOT   = "\x04" # 4  b10
  #
  CR    = 13
  LF    = 10
  ENQ   = 5
  ACK   = 6
  NACK  = 21
  STX   = 2
  ETX   = 3
  ETB   = 23
  EOT   = 4


  VT    = 11
  FS    = 28

  #LIAISON XL LAS PROTOCOL TESTS

  LSTX = 192
  LETX = 0

  #CENTAUR CONSTANTS
  CSTX  = 240
  CETX  = 248

end



