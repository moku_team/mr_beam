require 'fileutils'

class MrBeam::ActorLogger
  def initialize(subject, role, log_level = :info)
    @subject = subject
    @role = role
    @log_level = log_level || :info # Available levels: :none, :warn, :info, :debug, :trace
    @log_dir = 'log/mr_beam_log'

    FileUtils.mkdir_p(@log_dir) unless File.directory?(@log_dir)
  end

  def trace(message)
    return if @log_level != :trace

    File.write(get_file_name, build_log_message(message, :trace), mode: 'a')
  end

  def debug(message)
    return if @log_level != :debug && @log_level != :trace

    File.write(get_file_name, build_log_message(message, :debug), mode: 'a')
  end

  def info(message)
    return if @log_level != :info && @log_level != :debug && @log_level != :trace

    File.write(get_file_name, build_log_message(message, :info), mode: 'a')
  end

  def warn(message)
    return if @log_level == :none

    File.write(get_file_name, build_log_message(message, :warn), mode: 'a')
  end

  private

  def get_file_name
    "#{@log_dir}/#{@subject.gsub(/[^0-9A-Za-z_]/, '')}_#{@role.to_s}.log"
  end

  def build_log_message loggable, log_level
    msg = "#{log_level.to_s.upcase[0]}, [#{Time.now.strftime('%Y-%m-%dT%H:%M:%S.%L')}]"
    case log_level
      when :info
        msg << '  INFO'
      when :warn
        msg << '  WARN'
      when :debug
        msg << ' DEBUG'
      when :trace
        msg << ' TRACE'
    end

    if loggable.kind_of? Exception
      message = loggable.message.encode('utf-8', :invalid => :replace, :undef => :replace, :replace => '?')

      # The only case in which we won't add the exception backtrace is if we're handling a routine error with a log level lower than debug
      unless routine_errors.include?(loggable.class) && !([:debug, :trace].include? log_level)
        message += "\n#{loggable.backtrace.join("\n")}"
      end
    elsif @log_level == :trace
      # Print complete message and also non-visible characters.
      message = loggable.dump rescue ''
    else
      # I assume it's a string, but use the to_s method just in case and replace \r with \n for cosmetic purpose and easy debugging.
      message = loggable.to_s.gsub("\r\n", "\n").gsub("\r", "\n")
    end

    msg << " -- : #{message}\n"
  end

  # Routine errors have their backtrace logged only in debug and trace mode.
  def routine_errors
    [Errno::ECONNRESET, Errno::ETIMEDOUT, EOFError, ChannelErrors::IdleTimerExpired, Timeout::Error]
  end
end