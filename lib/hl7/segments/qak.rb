# Inject custom QAK segments in ruby-hl7.
require 'ruby-hl7'

class HL7::Message::Segment::QAK < HL7::Message::Segment
  weight 0
  add_field :query_tag              # query_id
  add_field :query_response_status  # e.g. 'OK'
  add_field :message_query_name     # e.g. 'Find Candidates'
  add_field :hit_count              # how many results  ~ limit. e.g. 50
  add_field :payload                # results
end
