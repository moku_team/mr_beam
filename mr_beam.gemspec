# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mr_beam/version'

Gem::Specification.new do |spec|
  spec.name          = 'mr_beam'
  spec.version       = MrBeam::VERSION
  spec.authors       = ['FabioRos, Szanella']
  spec.email         = ['fabio@moku.io, samuele@moku.io']

  spec.summary       = %q{Gem that connects stuff.}
  spec.description   = %q{Gem that allows to do things with some kind of connections through some things placed somewhere.}
  spec.homepage      = 'https://bitbucket.org/moku_team/mr_beam'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'state_pattern'
  spec.add_runtime_dependency 'ruby-hl7'
  spec.add_runtime_dependency 'require_all'
  spec.add_runtime_dependency 'celluloid',          '0.18.0'

  spec.add_development_dependency 'bundler',        '~> 1.12'
  spec.add_development_dependency 'rake',           '~> 10.0'

  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'factory_girl'

  spec.add_development_dependency 'faker'
  spec.add_development_dependency 'awesome_print'

  spec.add_runtime_dependency 'byebug'         #TODO DANGEROUS but cannot specify just development_dependency, since there is require byebug on top of many files.

  spec.add_development_dependency 'simplecov'


end
