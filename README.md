# MrBeam


![img](coverage/coverage.svg)

## Table of contents
[TOC]

## Introduction

MrBeam is a ruby gem designed to support medical communications.
At the moment only the HL7 and ASTM (packed and unpacked) protocols are supported.
The available transmission modes are:

* Socket
    * ASTM (single or double socket)
    * MLLP (double socket)
* Shared Folders
* FTP

Every channel acts as an endpoint that handles the connection independently using the selected protocol.
Any network failure in a channel is automatically and independently handled, without affecting the other channels.

All the interactions with MrBeam are performed through the CommunicationService,
a singleton object which acts as a container for the channels and as an API for the user.  

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'mr_beam'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install mr_beam

## Usage

Using MrBeam consists of two main activities: the channel configuration/setup and the transmission of messages.

### Channel configuration
As mentioned above, several types of channels can be used in MrBeam.

Any channel in your application should be added by passing a configuration hash to the `add_channel` method in the CommunicationService instance.
Each channel will be identified by a `subject`, determined at its creation, which needs to be used in order to transmit on the channel.  

While doing so, it's also possible to determine what operation will be executed once a message is received by the channel;
this can be performed by providing a block to the method invocation, which will receive the message string as a parameter and will be executed every time a message is received (acting as a callback).

The receive callback can return a hash containing options that MrBeam will use to determine how to behave after the message is received; further information can be found in each channel chapter.

The different channel types therefore share a few common points in their configurations, as follows:

* `type`: the channel type, either 'shared_folders', 'ftp' or 'socket'
* `subject`: the channel subject (or name)
* `config`: detailed configuration for the specific channel type
    * `log_level`: the log level to use for the channel (explained more in depth later)

The following snippet describes the usage of the `add_channel` method with a stub hash and a receive callback.
```ruby
config_hash = {
  type: 'your_channel_type',
  subject: 'your_subject',
  config: {
    log_level: 'info'
  }
}
MrBeam::CommunicationService.add_channel config_hash do |message|
  # Here I'm merely printing out the received message
  puts message
end
```

In the following sections I'll describe the possible options for each different channel type, while also providing a few useful examples.   

#### Shared folders
Shared folders channels transmit information through the exchange of files on specific folders.

Each channel is associated to an input and an output folder, where it will respectively look for input files and write its output files (with a configurable name).

The channel comes with a built-in configurable stability check for input files, which makes sure that the file content is not being altered during the reading procedure. Once an input file is read, MrBeam will delete it.

##### Available options
* `input_folder`: path of the channel input folder
* `output_folder`: path of the channel output folder
* `input_file_name`: name of the files that the channel will look for in the `input_folder`;  
the `*` wildcard can be used
* `output_file_name`: name of the output file, which will be written in the `output_folder`;  
it's possible to add the current timestamp to the name by using the `[TIMESTAMP]` wildcard
* `message_as_filename`: boolean that, if true, allows the message to be used as the file name instead of the file content. 
If enabled, the `output_file_name` option is ignored; default: `false`
* `should_overwrite_dest`: whether the channel should overwrite or not an output file already present in the output folder (true or false)
* `poll_interval`: how often (in seconds) the channel will poll its input folder and message queue; default: 1
* `matching_attempts`: number of consecutive times a file must not change its content to be considered valid; default: 4
* `read_attempt_interval`: time (in seconds) between each read attempt; default: 0.5
* `external_encoding`: encoding in which files are written and read by the channel; default: 'utf-8'

##### File stability
The next two options are needed to configure the file stability check.

To ensure that the file content doesn't change mid-read, it will perform several consecutive reads, one every `read_attempt_interval` seconds, until its content remains unvaried for `matching_attempts` times, or until 10 attempts are performed.

Fiddling with these parameters will change the channel reliability and speed, depending on your needs.

##### External encoding
Some shared folder channels will use different encodings from UTF-8 for their communication; if not handled, this will most likely lead to errors sooner or later. 
This encoding is called "external encoding", as opposed to the "internal encoding" used by MrBeam to handle strings.

For this reason, MrBeam allows the user to specify which encoding each shared folder channel will use with the `external_encoding` option, in order to be able to process files properly.

It's highly recommended to only specify the encoding when it's different from UTF-8.

##### Shared folders channel example
```ruby
config_hash = {
  type: 'shared_folders',
  subject: 'my_first_shared_folders_channel',
  config: {
    log_level: 'info',
    input_folder: 'your/input/folder',
    output_folder: 'your/output/folder',
    input_file_name: 'input.txt',
    output_file_name: 'output_[TIMESTAMP].txt',
    message_as_filename: false,
    should_overwrite_dest: false,
    poll_interval: 5,
    matching_attempts: 4,
    read_attempt_interval: 0.5,
    external_encoding: 'iso-8859-1'
  }
}
```

#### FTP
FTP channels work similarly to shared folders channels, except they use the FTP protocol in order to exchange files. Their configuration, therefore, includes all the data necessary to establish an FTP connection.
Moreover, FTP channels need a local `processing_folder` which they use to temporarily download and use the file.

FTP channels don't support the file stability function.

##### Available options
* `input_folder`: path of the channel input folder
* `output_folder`: path of the channel output folder
* `input_file_name`: name of the files that the channel will look for in the `input_folder`;  
the `*` wildcard can be used
* `output_file_name`: name of the output file, which will be written in the `output_folder`;  
it's possible to add the current timestamp to the name by using the `[TIMESTAMP]` wildcard
* `should_overwrite_dest`: whether the channel should overwrite or not an output file already present in the output folder (true or false)
* `processing_folder`: path of the processing folder
* `server`: address of the FTP server
* `port`: port of the FTP server
* `username`: username for the FTP server
* `password`: password for the FTP server

##### FTP channel example
```ruby
config_hash = {
  type: 'ftp',
  subject: 'my_first_shared_folders_channel',
  config: {
    log_level: 'info',
    input_folder: 'your/input/folder',
    output_folder: 'your/output/folder',
    input_file_name: 'input.txt',
    output_file_name: 'output_[TIMESTAMP].txt',
    should_overwrite_dest: false,
    processing_folder: 'your/processing/folder',
    server: '127.0.0.1',
    port: '21',
    username: 'username',
    password: 'password'
  }
}
```

#### ASTM Socket
MrBeam supports two protocols for socket channels. The first one is ASTM, in its `Packed` and `Unpacked` modes.
It's possible to either use a single socket, as described in the ASTM protocol, or two separate sockets, with one acting as a receiver and the other as a sender.

In the default `Unpacked` mode, ASTM messages are split in rows, which are sent one at a time. On the other hand, the `Packed` mode has the whole message sent in one single transmission.

Each socket can be instantiated either as a server or a client; please note that **only one** connection at a time is supported, and this implies that the socket server will be closed after the first client connection.

##### Idle timer
Some ASTM socket implementations are frail, to put it mildly. Some won't be able to recover from connection errors, and some will stop working properly after being idle for a while.

As an attempt to prevent issues on such channels, MrBeam allows channels to be reset after being idle for a configurable amount of time. 
This can be done by adding the `idle_timer` option to the channel configuration, which contains the number of consecutive minutes in idle after which the channel will be reset.
By either skipping this option or setting it to zero, this function will be disabled. 

##### Available options
* `protocol_config`: a hash that contains the following options:
    * `protocol_name`: in this case, 'astm'
    * `protocol_mode`: either 'Packed' or 'Unpacked'
* `idle_timer`: the idle timer in minutes
* `mode`: either 'server' or 'client'
* `address`: the server to connect to (only for clients)
* `port`: either the port to connect to (for clients) or the listen port (for servers)
* `sender`, `receiver`: two hashes containing the configurations of the sockets in a double-socket channel, as follows:
    * `mode`, `address`, `port`: as described above

To determine whether to use a single or double socket, MrBeam checks for the presence of the `sender` and `receiver` hashes:
if they're present, it'll instantiate a double socket channel with the configurations specified in the two sub-hashes, while **ignoring** the configuration described in the parent hash (which should be skipped altogether);
otherwise, it'll use the configuration in the main hash to instantiate a single socket channel.

##### Single socket channel example (Unpacked mode, server)
```ruby
config_hash = {
  type: 'socket',
  subject: 'my_first_astm_socket_channel',
  protocol_config: {
    protocol_name: 'astm',
    protocol_mode: 'Unpacked'
  },
  config: {
    log_level: 'debug',
    idle_timer: 5,
    mode: 'server',
    port: 2100
  }
}
```

##### Double socket channel example (Packed mode, server+client)
```ruby
config_hash = {
  type: 'socket',
  subject: 'my_second_astm_socket_channel',
  protocol_config: {
    protocol_name: 'astm',
    protocol_mode: 'Packed'
  },
  config: {
    idle_timer: 5,
    log_level: 'trace',
    receiver: {
      mode: 'server',
      port: 2100
    },
    sender: {
      mode: 'client',
      address: '127.0.0.1',
      port: 2101
    }
  }
}
```
In most real-life use cases, in a double socket configuration, receivers will be servers and senders will be clients.

#### HL7 Socket
The second supported socket protocol is HL7. Similarly to the ASTM one, this socket channel can be used in either single or double socket mode.

This type of channel supports the idle timer option exactly as the ASTM socket channel.

The receive callback for this channel can be used to determine a custom ACK response for the received transmissions.

##### Available options
* `protocol_config`: a hash that contains the following options:
    * `protocol_name`: in this case, 'hl7'
* `idle_timer`: the idle timer in minutes
* `sender`, `receiver`: two hashes containing the configurations of the sockets, as follows:
    * `mode`: either 'server' or 'client'
    * `address`: the server to connect to (only for clients)
    * `port`: either the port to connect to (for clients) or the listen port (for servers)
    
##### Available receive callback options
* `response`: a `HL7::Message` object containing the ACK response 

##### HL7 double channel example (MLLP, server+client) 
```ruby
config_hash = {
  type: 'socket',
  subject: 'my_first_mllp_channel',
  protocol_config: {
    protocol_name: 'hl7'
  },
  config: {
    log_level: 'info',
    idle_timer: 4,
    receiver: {
      mode: 'server',
      port: 2100
    },
    sender: {
      mode: 'client',
      address: '127.0.0.1',
      port: 2101
    }
  }
}
```

##### HL7 single channel example (server)
```ruby
config_hash = {
  type: 'socket',
  subject: 'my_first_hl7_single_channel',
  protocol_config: {
    protocol_name: 'hl7'
  },
  config: {
    log_level: 'info',
    priority_owner_branch: "receiving",
    idle_timer: 10,
    port: "3120",
    mode: "server"
  }
}
```

### Transmission
Once all the channels are configured, **provided the main thread remains alive**, they will autonomously receive messages from their source, as well as handle connection failures.
It's also possible to transmit messages through them, which can be done through the CommunicationService instance previously used to add them.

The method to use is called `transmit`, and it accepts three parameters:

* `message`, a string containing the message to transmit
* `subject`, the subject of the channel to transmit the message to
* a block, which will be executed once the message is either sent successfully, or fails.

The following snippet contains and example invocation of the method.

```ruby
message = 'I am a message'
MrBeam::CommunicationService.instance.transmit message, 'channel_subject' do |outcome|
  if outcome.success?
    puts "Your message was sent successfully!"
  else
    puts 'Errors encountered while sending your message:'
    outcome.errors.map { |error| puts error }
  end
end
```
As shown in the example, the transmit callback receives an `outcome` parameter, which allows you to determine whether the transmission was successful or not and to act accordingly.
It provides a `success?` method, which returns true if the transmission was successful and false otherwise, and an `errors` method, which returns an array of strings which describe what errors occurred.

### Error handling
You may want to use a notifier to be aware of any errors happening in your application and in MrBeam. To achieve this, MrBeam provides the `exception_handler` method, which receives a block. The block will be executed at every exception, which it receives as a parameter.

The following snippet portrays an example usage of the method.

```ruby
MrBeam::CommunicationService.instance.exception_handler { |ex| Airbrake.notify_sync(ex) }
```

### Logging
MrBeam comes with a built-in configurable logging system. Log files will be written in the `log/mr_beam_log` folder, **one file per actor**. What I mean by actor is, in a channel, an independent direction of the communication. For instance, a shared folders channel contains two actors, one monitoring the output folder and the other processing the message queue.

For each channel, there are 5 possible log levels:

* `none`: no logging
* `warn`: errors only
* `info`: high level informational messages
* `debug`: fine-grained messages useful to debug the application
* `trace`: finer-grained messages than the `debug` level

To determine the log level of a channel, use the `log_level` field of the configuration hash, as described in the Configuration chapter.

### Connection status
An application implementing communication between several devices will most likely want to be aware of the connection status of its channels. MrBeam's CommunicationService provides a method that returns a snapshot of the connection statuses when invoked, called `channel_status`.

The method accepts an optional parameter, `subject`, in case the user only needs to know the status of a specific channel, and it returns an array of hashes structured as follows.

```ruby
[
  {
    subject: 'channel_1',
    inbound: 'connected',
    outbound: 'connecting'
  },
  {
    subject: 'channel_2',
    inbound: 'error',
    outbound: 'connected'
  }
]
```
Each hash represents a single channel, and contains information about its inbound (receiving) and outbound (sending) directions. 
The connection status hashes are always double, even in duplex channels: this allows the application to be agnostic about the underlying architecture, and to always treat channels as a dual stream. 

The possible status values are:

* `'initializing'`: this means that the channel is starting and has yet to establish a connection
* `'connecting'`: the channel has established a connection, but no message has been sent yet
* `'connected'`: the channel is connected and at least one message has been exchanged
* `'error'`: the channel has been disconnected

### Termination
When terminating an application that uses MrBeam, it's advised not to do so in an abrupt fashion, as this does not allow channels to be terminated gracefully and might lead to communication problems. To command MrBeam to terminate its channels, the CommunicationService instance provides a `stop_all` method, which accepts the optional parameter `timeout` (in seconds, default value: 60).

The `stop_all` method is **synchronous**, meaning that it will only terminate once all channels are stopped, or when the timeout expires before all channels terminate cleanly. If the timeout expires, the `ChannelErrors::ChannelStopTimeOut` will be raised.

```ruby
  # With the timeout parameter
  MrBeam::CommunicationService.instance.stop_all 90
  # Without the timeout parameter
  MrBeam::CommunicationService.instance.stop_all
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/moku_team/mr_beam. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
